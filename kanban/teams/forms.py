__author__ = 'Pavilion'

#Forms for maintaining development team

from django import forms
from django.db import models
from django.core.exceptions import ValidationError
from kanban.models import Team
from django.contrib.auth.models import User
import copy


USERS = ((user.id, user.first_name+" "+user.last_name+"-"+user.username) for user in User.objects.all())


def team_name_taken(name):
    if Team.objects.filter(team_name=name):
        raise ValidationError('Team name already taken')

def get_users(users_id):
    all_users = [User.objects.get(pk=id) for id in users_id]
    return ((user.id, user.first_name+" "+user.last_name+"-"+user.username) for user in all_users)





"""




#Form for creating new team
class AddTeamForm(forms.Form):
    team_name = forms.CharField(max_length=100, validators=[team_name_taken])




class AddTeamMember(forms.Form):
    member = forms.CharField(max_length=100)

"""

class CreateTeamForm(forms.Form):
    team_name = forms.CharField(max_length=100, validators=[team_name_taken])
    members = forms.MultipleChoiceField(choices=USERS)


class AssignRoles(forms.Form):

    def __init__(self, *args, **kwargs):
        self.team_members_id = kwargs.pop('team_members_id')
        super(AssignRoles, self).__init__(*args, **kwargs)

        self.fields['product_owner'] = forms.ChoiceField(choices=get_users(self.team_members_id))
        self.fields['kanban_master'] = forms.ChoiceField(choices=get_users(self.team_members_id))
        self.fields['developers'] = forms.MultipleChoiceField(choices=get_users(self.team_members_id))


    def clean(self):
        cleaned_data = super(AssignRoles, self).clean()
        product_owner = cleaned_data.get('product_owner')
        kanban_master = cleaned_data.get('kanban_master')
        developers = cleaned_data.get('developers')

        if not developers:
            #raise ValidationError("Have to pick at least one developer!")
            pass
        else:
            team_members = self.team_members_id

            if product_owner in team_members:
                team_members.remove(product_owner)

            if kanban_master in team_members:
                team_members.remove(kanban_master)

            for developer in developers:
                if developer in team_members:
                    team_members.remove(developer)

            if team_members:
                raise ValidationError("Not all members have role assigned!")

        return self.cleaned_data

