from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
import forms
from django.contrib.auth.models import User
from kanban.models import *
import copy
# Create your views here.

"""
def add_team(request):

    if request.method == 'POST':
        form = forms.AddTeamForm(request.POST)
        if form.is_valid():

            add_member_form = forms.AddTeamMember()
            request.session['team_name'] = form.cleaned_data['team_name']
            request.session['team_members'] = []

            return render_to_response('teams/create_team.html', {"submitted":"submitted", "add_member_form":add_member_form}, context_instance=RequestContext(request))
        else:
            return render_to_response('teams/create_team.html', {"form":form}, context_instance=RequestContext(request))

    else:
        form = forms.AddTeamForm()
        return render_to_response('teams/create_team.html', {"form": form}, context_instance=RequestContext(request))


def find_members(request):

    if request.method == 'POST':
        add_member_form = forms.AddTeamMember(request.POST)

        if add_member_form.is_valid():
            user_input = add_member_form.cleaned_data['member']

            users_by_name = User.objects.filter(first_name__contains=user_input)
            users_by_surname = User.objects.filter(last_name__contains=user_input)
            users_by_username = User.objects.filter(username=user_input)

            users = list(set(users_by_name) | set(users_by_surname) | set(users_by_username))
            add_member_form = forms.AddTeamMember()

            if users:
                return render_to_response('teams/create_team.html', {"submitted":"submitted", "users":users, "add_member_form":add_member_form, "users_added":request.session['team_members']}, context_instance=RequestContext(request))
            else:
                return render_to_response('teams/create_team.html', {"submitted":"submitted", "error":"No users found", "add_member_form":add_member_form, "users_added":request.session['team_members']}, context_instance=RequestContext(request))
        else:
            return render_to_response('teams/create_team.html', {"submitted":"submitted", "add_member_form":add_member_form, "users_added":request.session['team_members']}, context_instance=RequestContext(request))

    else:
         add_member_form = forms.AddTeamMember()
         return render_to_response('teams/create_team.html', {"submitted":"submitted", "add_member_form":add_member_form, "users_added":request.session['team_members']}, context_instance=RequestContext(request))




def add_member(request, user_id):

    user = User.objects.get(pk=int(user_id))
    users = request.session['team_members']
    if user not in users:
        users.append(user)
    request.session['team_members'] = users
    add_member_form = forms.AddTeamMember()
    return render_to_response('teams/create_team.html', {"submitted":"submitted", "add_member_form":add_member_form, "users_added":request.session['team_members']}, context_instance=RequestContext(request))


def remove_member(request, user_id):
    user = User.objects.get(pk=int(user_id))
    users = request.session['team_members']
    users.remove(user)
    request.session['team_members'] = users
    add_member_form = forms.AddTeamMember()
    return render_to_response('teams/create_team.html', {"submitted":"submitted", "add_member_form":add_member_form, "users_added":request.session['team_members']}, context_instance=RequestContext(request))
    """

def create_team(request):

    if request.method == 'POST':
        form = forms.CreateTeamForm(request.POST)
        if form.is_valid():
            team_name = form.cleaned_data['team_name']
            team_members_id = form.cleaned_data['members']
            #team_members = [User.objects.get(pk=int(id)) for id in team_members_id]
            #request.session['team_members'] = team_members
            request.session['team_name'] = team_name
            request.session['team_members_id'] = team_members_id
            #roles = Role.objects.all()

            assign_role_form = forms.AssignRoles(team_members_id=team_members_id)
            return render_to_response('teams/create_team.html', {"ok":"ok", "assign_role_form":assign_role_form, "team_name":team_name}, context_instance=RequestContext(request))
            #return render_to_response('teams/create_team.html', {"ok":"ok", "team_name": team_name, "team_members":team_members, "roles":roles}, context_instance=RequestContext(request))
        else:
            return render_to_response('teams/create_team.html', {"create_team_form": form}, context_instance=RequestContext(request))

    form = forms.CreateTeamForm()
    return render_to_response('teams/create_team.html', {"create_team_form": form}, context_instance=RequestContext(request))


def assign_roles(request):

    if request.method == 'POST':
        team_members_id = request.session['team_members_id']
        assign_role_form = forms.AssignRoles(request.POST, team_members_id=copy.copy(team_members_id))

        #Is valid - create team
        if assign_role_form.is_valid():





            kbm_role = Role.objects.get(role_name="KanbanMaster")
            po_role = Role.objects.get(role_name="ProductOwner")
            dp_role = Role.objects.get(role_name="Developer")

            product_owner_id = assign_role_form.cleaned_data['product_owner']
            kanban_master_id = assign_role_form.cleaned_data['kanban_master']
            developers_id = assign_role_form.cleaned_data['developers']


            team_name = request.session['team_name']
            team = Team.objects.create(team_name=team_name)
            team.save()

            for team_member_id in team_members_id:
                member = User.objects.get(pk=team_member_id)
                member_team = MemberTeam(member=member, team=team, is_active=True)
                member_team.save()
                if team_member_id==product_owner_id:
                    member_team_role_1 = MemberTeamRole(member_team=member_team, role=po_role)
                    member_team_role_1.save()

                if team_member_id==kanban_master_id:
                    member_team_role_2 = MemberTeamRole(member_team=member_team, role=kbm_role)
                    member_team_role_2.save()

                if team_member_id in developers_id:
                    member_team_role_3 = MemberTeamRole(member_team=member_team, role=dp_role)
                    member_team_role_3.save()



            return render_to_response('teams/create_team.html', {"ok":"ok", "assign_role_form":assign_role_form, "message":"Team has been successfully created!", "tmi":developers_id}, context_instance=RequestContext(request))

        else:

            return render_to_response('teams/create_team.html', {"ok":"ok", "assign_role_form":assign_role_form, "message":"Team has not been successfully created!"}, context_instance=RequestContext(request))


    return render_to_response('teams/create_team.html', context_instance=RequestContext(request))




