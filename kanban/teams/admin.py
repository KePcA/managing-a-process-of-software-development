from django.contrib import admin

# Register your models here.
from kanban.models import Team, MemberTeam, Role, MemberTeamRole, MemberTeamActivity, \
    UserStoryCard, UserProfile, Board, Column, Subcolumn, SwimLane, Project, WIPViolation, CardMovement

admin.site.register(Team)
admin.site.register(MemberTeam)
admin.site.register(Role)
admin.site.register(MemberTeamRole)
admin.site.register(MemberTeamActivity)
admin.site.register(UserStoryCard)
admin.site.register(UserProfile)
admin.site.register(Board)
admin.site.register(Column)
admin.site.register(Subcolumn)
admin.site.register(SwimLane)
admin.site.register(Project)
admin.site.register(WIPViolation)
admin.site.register(CardMovement)