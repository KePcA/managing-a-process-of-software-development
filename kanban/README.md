Superuser za bazo:
User: admin
Geslo: 16smrpo


USTVARJANJE RAZVOJNE SKUPINA:
-razvojno skupino lahko ustvari samo član, ki je usposoblen ja kanban masterja (in administrator)
-določi se kanban master, product owner in razvijalci (na voljo so le tisti, ki so za določeno vlogo usposobljeni)

 POSODABLJANJE RAZVOJNE SKUPINE
 -zamenjaš lahko vloge kanban masterja, product ownerja, odstraniš in dodaš razvijalce
 -pustiš pri miru tista polja, kjer ne želiš sprememb
 -če uporabniku želiš dodati/odstraniti vlogo(v okviru njegove uposobljenosti), ga izbereš/odstraniš in polja
 -če uporabniku odstraniš vse vloge, sistem avtomatsko poskrbi za "brisanje" iz skupine

 USTVARJANJE IN POSODABLJANJE PROJEKTOV
 -Ko je projekt ustvarjen, se avtomatsko nastavi da je aktiven in da se delo na njem še ni pričelo
 - !!! Atribut work_started - nastavi na True, takrat ko se prične delo na projektu (to je takrat, ko že obstajajo kartice)

 TABLE
 - Vsak column ima vedno vsaj en subcolumn. Če ima samo enega, se ta ne prikaže, drugače se prikažejo vsi.