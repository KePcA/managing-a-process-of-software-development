from django.conf.urls import patterns, include, url
from kanban.views import UserLogin, UserRegister, CreateUserStoryCard, BoardsList, DisplayBoard, Users, AddUser, \
    EditUser, CreateBoard, CreateColumn, Help, EditCard, AverageLeadTime, CumulativeFlowDiagram, WipList, TaskDistributionDiagram
from kanban.create_database import PopulateDatabase, PopulateDatabaseTeamBoard, PopulateCardMovements
from datetime import datetime
from apscheduler.scheduler import Scheduler
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
import kanban.utilities
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'kanban.views.home', name='home'),

    #user authentication
    url(r'^accounts/login/$', UserLogin.as_view(), name='login'),
    url(r'^accounts/register/', UserRegister.as_view(), name='register'),
    url(r'^accounts/logout', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),

    #user maintance
    url(r'^users/$', Users.as_view(), name='users'),
    url(r'^users/add_user$', AddUser.as_view(), name='add_user'),
    url(r'^users/edit_user/(?P<user_id>\d+)/', EditUser.as_view(), name='edit_user'),
    url(r'^accounts/register/', UserRegister.as_view(), name='register'),
    url(r'^accounts/logout', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),

    #documentation
    url(r'^help/$', Help.as_view(), name='help'),

    #development teams maintenance
    #url(r'^teams/add_team/$', 'teams.views.add_team', name='add_team'),
    #url(r'^teams/add_team/find_members$', 'teams.views.find_members', name='find_members'),
    #url(r'^teams/add_team/find_members/(?P<user_id>[0-9]+)$', 'teams.views.add_member', name='add_member'),
    #url(r'^teams/add_team/remove_member/(?P<user_id>[0-9]+)$', 'teams.views.remove_member', name='remove_member'),


    #creating and updating teams
    url(r'^teams/$', 'kanban.views.team', name='team'),
    url(r'^teams/create_team/$', 'kanban.views.create_team', name='create_team'),
    url(r'^teams/(?P<team_id>[0-9]+)$', 'kanban.views.show_team', name='show_team'),
    url(r'^teams/(?P<team_id>[0-9]+)/update_team/$', 'kanban.views.update_team', name='update_team'),
    url(r'^teams/activity/$', 'kanban.views.team_activity', name='activity'),
    #url(r'^teams/create_team/roles$', 'teams.views.assign_roles'),

    #creating and updating projects
    url(r'^projects/$', 'kanban.views.project', name='project'),
    url(r'^projects/create_project/$', 'kanban.views.create_project', name='create_project'),
    url(r'^projects/(?P<project_id>[0-9]+)/edit_project/$', 'kanban.views.edit_project', name='edit_project'),
    url(r'^projects/(?P<project_id>[0-9]+)/deactivate/$', 'kanban.views.deactivate_project', name='deactivate_project'),
    url(r'^projects/(?P<project_id>[0-9]+)/activate/$', 'kanban.views.activate_project', name='activate_project'),


    #user story cards
    url(r'^boards/display_board/(?P<board_id>\d+)/create_card/(?P<card_type>\w+)$', CreateUserStoryCard.as_view(), name='create_card'),
    url(r'^boards/display_board/(?P<board_id>\d+)/create_card/violation/$',
        'kanban.views.confirm_wip_violation_and_create_card', name='confirm_wip_violation_and_create_card'),
    url(r'^boards/display_board/edit_card/(?P<card_id>\d+)$', EditCard.as_view(), name='edit_card'),
    url(r'^boards/display_board/(?P<board_id>\d+)/move_card/(?P<card_id>\d+)/(?P<direction>\w+)$', 'kanban.views.move_card', name='confirm_wip_violation_and_create_card'),
    url(r'^boards/display_board/(?P<board_id>\d+)/move_card/(?P<card_id>\d+)/(?P<direction>\w+)$', 'kanban.views.move_card', name='move_card'),
    url(r'^boards/display_board/(?P<board_id>\d+)/move_card/violation/(?P<card_id>\d+)/(?P<subcolumn_id>\d+)$', 'kanban.views.confirm_wip_violation', name='confirm_wip_violation'),
    url(r'^boards/display_board/(?P<board_id>\d+)/decline_story/(?P<card_id>\d+)$', 'kanban.views.decline_story', name='decline_story'),
    url(r'^boards/display_board/(?P<board_id>\d+)/card_movement_permission/$', 'kanban.views.card_movement_permission', name='card_movement_permission'),
    url(r'^boards/display_board/(?P<board_id>\d+)/card_movement_permission/add/$', 'kanban.views.add_card_movement_permission', name='add_card_movement_permission'),
    url(r'^boards/display_board/(?P<board_id>\d+)/card_movement_permission/(?P<card_movement_permission_id>\d+)/edit/$', 'kanban.views.edit_card_movement_permission', name='edit_card_movement_permission'),
    url(r'^boards/display_board/(?P<board_id>\d+)/card_movement_permission/(?P<card_movement_permission_id>\d+)/remove/$', 'kanban.views.remove_card_movement_permission', name='remove_card_movement_permission'),
    url(r'^cards/(?P<card_id>\d+)/card_details/$', 'kanban.views.card_details', name='card_details'),
    url(r'^cards/(?P<card_id>\d+)/delete_card/$', 'kanban.views.delete_card', name='delete_card'),
    url(r'^cards/(?P<card_id>\d+)/add_task/$', 'kanban.views.add_task', name='add_task'),
    url(r'^cards/(?P<card_id>\d+)/edit_task/(?P<task_id>\d+)/$', 'kanban.views.edit_task', name='edit_task'),
    url(r'^cards/(?P<card_id>\d+)/delete_task/(?P<task_id>\d+)/(?P<to_redir>\d+)/$', 'kanban.views.delete_task', name='delete_task'),

    # Boards.
    url(r'^boards/$', BoardsList.as_view(), name='boards'),
    url(r'^boards/display_board/(?P<board_id>\d+)$', DisplayBoard.as_view(), name='display_board'),
    url(r'^boards/display_board/(?P<board_id>\d+)/properties$', 'kanban.views.board_properties', name='board_properties'),
    url(r'^boards/create_board/$', CreateBoard.as_view(), name='create_board'),
    url(r'^boards/copy_board_structure/(?P<board_id>\d+)$', 'kanban.views.copy_board_structure', name='copy_board_structure'),
    url(r'^boards/create_column/(?P<board_id>\d+)/(?P<order>\d+)$', CreateColumn.as_view(), name='create_column'),
    url(r'^boards/update_column/(?P<column_id>\d+)$', 'kanban.views.update_column', name='update_column'),
    url(r'^boards/update_column/violation/(?P<column_id>\d+)/(?P<new_name>[\w ]+)/(?P<new_wip_limit>\d+)$', 'kanban.views.update_column_violation', name='update_column_violation'),
    url(r'^boards/collapse_column/(?P<column_id>\d+)$', 'kanban.views.collapse_column', name='collapse_column'),
    url(r'^boards/delete_column/(?P<column_id>\d+)$', 'kanban.views.delete_column', name='delete_column'),
    url(r'^boards/create_swim_lane/(?P<board_id>\d+)$', 'kanban.views.create_swim_lane', name='create_swim_lane'),
    url(r'^boards/delete_swim_lanes/(?P<board_id>\d+)$', 'kanban.views.delete_swim_lanes', name='delete_swim_lanes'),
    url(r'^boards/delete_swim_lane/(?P<swim_lane_id>\d+)$', 'kanban.views.delete_swim_lane', name='delete_swim_lane'),
    url(r'^boards/create_subcolumn/(?P<column_id>\d+)/(?P<order>\d+)$', 'kanban.views.create_subcolumn', name='create_subcolumn'),
    url(r'^boards/update_subcolumn/(?P<subcolumn_id>\d+)$', 'kanban.views.update_subcolumn', name='update_subcolumn'),
    url(r'^boards/delete_subcolumn/(?P<subcolumn_id>\d+)$', 'kanban.views.delete_subcolumn', name='delete_subcolumn'),

    #WIP violations
    url(r'^wip_list/(?P<board_id>\d+)$', WipList.as_view(), name='wip_list'),

    #Average lead time
    url(r'^boards/average_lead_time/(?P<board_id>\d+)$', AverageLeadTime.as_view(), name='average_lead_time'),

    # Cumulative flow diagram
    url(r'^boards/cumulative_flow_diagram/(?P<board_id>\d+)$', CumulativeFlowDiagram.as_view(),
        name='cumulative_flow_diagram'),

    #Task distribution diagram
    url(r'^boards/task_distribution_diagram/(?P<board_id>\d+)$', TaskDistributionDiagram.as_view(),
        name='task_distribution_diagram'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Populate database
    url(r'^populate_database$', PopulateDatabase.as_view(), name='populate_database'),
    url(r'^populate_database_team_board$', PopulateDatabaseTeamBoard.as_view(), name='populate_database_teams_board'),
    url(r'^populate_card_movements$', PopulateCardMovements.as_view(), name='populate_card_movements')

)

# This is here because urls.py execute once and once only.

sched = Scheduler()
sched.start()

sched.add_cron_job(kanban.utilities.send_mail_reminder, day_of_week='mon-sun', hour=0, minute=5)
