from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import datetime

# Create your models here.

#
# USER RELATED MODELS
#

#Extended user - add fields if your app needs them
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    can_be_kbm = models.BooleanField()
    can_be_po = models.BooleanField()
    can_be_dp = models.BooleanField()

    class Meta:
        db_table = 'user_profile'

    def __unicode__(self):
        return self.user.username

#Model, which represents team - many to many relation to User
class Team(models.Model):
    team_name = models.CharField(max_length=200)
    members = models.ManyToManyField(UserProfile, through='MemberTeam')

    class Meta:
        db_table = 'team'

    def __unicode__(self):
        return self.team_name

#Model containing information about particular user being member in particular team
class MemberTeam(models.Model):
    id = models.AutoField(primary_key=True)
    member = models.ForeignKey(UserProfile)
    team = models.ForeignKey(Team)
    is_active = models.BooleanField()
    date_added = models.DateTimeField(default=timezone.now())

    class Meta:
        db_table = 'member_team'

    def __str__(self):
        return self.member.user.username +" " +self.team.team_name

#Model which contains all the possible roles available - they are KanbanMaster, ProductOwner, Developer
class Role(models.Model):
    id = models.AutoField(primary_key=True)
    role_name = models.CharField(max_length=100)
    member_team_roles = models.ManyToManyField(MemberTeam, through='MemberTeamRole')

    class Meta:
        db_table = 'role'

    def __str__(self):
        return self.role_name

#Model which contains roles of particular member in particular team
class MemberTeamRole(models.Model):
    id = models.AutoField(primary_key=True)
    member_team = models.ForeignKey(MemberTeam)
    role = models.ForeignKey(Role)

    class Meta:
        db_table = 'member_team_role'

    def __str__(self):
        return "Team: " +self.member_team.team.team_name +" role: " +self.role.role_name

#Model which contains information about period of activity of particular member in particular team - member was active from active_from until active_until(can be null, member stil active)
class MemberTeamActivity(models.Model):
    id = models.AutoField(primary_key=True)
    member_team = models.ForeignKey(MemberTeam)
    active_from = models.DateTimeField('active_from', default=timezone.now())
    active_until = models.DateTimeField('active_until', blank=True, null=True)

    class Meta:
        db_table = 'member_team_activity'

#
# BOARD RELATED MODELS
#

class Board(models.Model):
    name = models.CharField(max_length=200)
    should_display_priority = models.BooleanField(default=False)
    should_display_points = models.BooleanField(default=False)
    should_display_date_to_finish = models.BooleanField(default=False)
    n_days_reminder = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'board'

class Column(models.Model):
    board = models.ForeignKey(Board)
    order = models.IntegerField()
    name = models.CharField(max_length=200)
    wip_limit = models.PositiveIntegerField(null=True, blank=True)
    collapsed = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'column'

class Subcolumn(models.Model):
    column = models.ForeignKey(Column)
    order = models.IntegerField()
    name = models.CharField(max_length=200, null=False, blank=False, default="Default Subcolumn")
    is_border_column = models.BooleanField(default=False)
    is_highest_priority_card_column = models.BooleanField(default=False)
    is_acceptance_testing_column = models.BooleanField(default=False)

    def __str__(self):
        return self.column.name + ": " + self.name

    class Meta:
        db_table = 'subcolumn'
        ordering = ['order']

"""
class Project(models.Model):
    code = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    client_name = models.CharField(max_length=100)
    start_date = models.DateField()
    anticipated_end_date = models.DateField()
    team = models.ForeignKey(Team, null=True) # Remove null after some teams are created in create_database.

    class Meta:
        db_table = 'project'
"""

class Project(models.Model):
    code = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    client_name = models.CharField(max_length=100)
    start_date = models.DateField()
    anticipated_end_date = models.DateField()
    finished_date = models.DateField(blank=True, null=True)
    team = models.ForeignKey(Team)
    is_active = models.BooleanField(default=True)
    work_started = models.BooleanField(default=False)

    class Meta:
        db_table = 'project'

    def __str__(self):
        return self.title


class SwimLane(models.Model):
    project = models.OneToOneField(Project, related_name='swim_lane')
    board = models.ForeignKey(Board)
    order = models.IntegerField(null=False, blank=False)

    class Meta:
        db_table = 'swim_lane'

#
# CARD RELATED MODELS
#

CARD_TYPES = (
    ('nc', 'Normal card'),
    ('sb', 'Silver bullet'),
    ('rc', 'Rejected card')
)

CARD_PRIORITIES = {
    ('low', 'Low'),
    ('med', 'Medium'),
    ('high', 'High')
}


#Model for a card that represents one user story.
class UserStoryCard(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    subcolumn = models.ForeignKey(Subcolumn)
    project = models.ForeignKey(Project)
    type = models.CharField(max_length=100, choices=CARD_TYPES)
    priority = models.CharField(max_length=100, choices=CARD_PRIORITIES)
    points = models.DecimalField(decimal_places=2, max_digits=5)
    date_created = models.DateTimeField(default=datetime.datetime.today())
    development_started = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    deletion_reason = models.CharField(max_length=200, null=True)
    responsible_person = models.ForeignKey(UserProfile, null=True)
    date_to_finish = models.DateField(null=True)

    class Meta:
        db_table = 'user_story_card'

    def __unicode__(self):
        return self.title




#
# WIP violation RELATED MODELS
#

#Model for work in progress (WIP) violations
WIP_REASONS = (
    ('cm', 'Card movement'),
    ('cc', 'Card creation'),
)
class WIPViolation(models.Model):
    card = models.ForeignKey(UserStoryCard)
    date = models.DateField()
    column = models.ForeignKey(Column)
    user = models.ForeignKey(User)
    reason = models.CharField(max_length=100, choices=WIP_REASONS)

    class Meta:
        db_table = 'wip_violation'

    def __unicode__(self):
        return self.card.title +", " +str(self.date) +" violation."


class WIPColumnViolation(models.Model):
    column = models.ForeignKey(Column)
    date = models.DateField()
    user = models.ForeignKey(User)

    class Meta:
        db_table = 'wip_column_violation'

    def __unicode__(self):
        return self.column.name + ", "+ self.date + " column WIP violation."
    
class CardMovement(models.Model):
    card = models.ForeignKey(UserStoryCard)
    column_from = models.ForeignKey(Column, related_name='column_from')
    column_to = models.ForeignKey(Column, related_name='column_to')
    user = models.ForeignKey(User)
    date = models.DateTimeField(default=datetime.datetime.today())

    class Meta:
        db_table = 'card_movement'

    def __str__(self):
        return self.card.title + 'from ' + self.column_from.name + ' to ' + self.column_to.name

class CardMovementPermission(models.Model):
    subcolumn_from = models.ForeignKey(Subcolumn, related_name='subcolumn_from')
    subcolumn_to = models.ForeignKey(Subcolumn, related_name='subcolumn_to')
    po_allowed = models.BooleanField(default=True)
    kbm_allowed = models.BooleanField(default=True)
    dp_allowed = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'card_movement_permission'


class VisualSettings(models.Model):
    font_size_const = models.IntegerField()

    class Meta:
        db_table = 'visual_settings'


class Task(models.Model):
    description = models.CharField(max_length=1000)
    hours = models.PositiveIntegerField()
    completed = models.BooleanField(default=False)
    card = models.ForeignKey(UserStoryCard)

    class Meta:
        db_table = 'task'
