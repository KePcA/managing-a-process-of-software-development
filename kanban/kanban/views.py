from django.shortcuts import render_to_response, redirect, render
from django.contrib.auth.decorators import login_required
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate, login
from django.db.models import F, Max
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from lockout import LockedOut
from django.template import RequestContext
from models import *
import forms
from django import forms as django_forms
import utilities
from django.http import HttpResponse
from decorators import access_required
from django.db import transaction
import json

@login_required
def home(request):
    return render_to_response('index.html', RequestContext(request))


class UserLogin(View):
    template_name = 'registration/login.html'

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]

        if len(username) < 4:
            return render(request, self.template_name, {"error": "Username should be at least 4 characters long!"})

        if len(password) < 4:
            return render(request, self.template_name, {"error": "Password should be at least 4 characters long!"})
        try:
            user = authenticate(username=username, password=password)
        except LockedOut:
            return render(request, self.template_name,
                          {"error": 'Your account has been locked out because of too many failed login attempts.'})
        if user is not None:
            if user.is_active:
                login(request, user)
                request.session['user'] = user
                return redirect('home')
            else:
                return render(request, self.template_name, {"error": "Your account is not active!"})
        else:
            return render(request, self.template_name, {"error": "Inavlid login! Please try again."})

    def get(self, request):
        return render(request, self.template_name)


class UserRegister(View):
    template_name = 'registration/register.html'

    def post(self, request):
        email = request.POST["username"]
        username = email
        password = request.POST["password"]
        password_repeat = request.POST["password_repeat"]

        try:
            validate_email(email)
        except ValidationError:
            return render(request, self.template_name, {"error": "Email not valid!"})

        if len(username) < 4:
            return render(request, self.template_name, {"error": "Username should be at least 4 characters long!"})

        if len(password) < 4:
            return render(request, self.template_name, {"error": "Password should be at least 4 characters long!"})

        if not password == password_repeat:
            return render(request, self.template_name, {"error": "Passwords did not match!"})

        if not User.objects.filter(username=username).count():
            User.objects.create_user(username=username, password=password)
            user = authenticate(username=username, password=password)
            UserProfile.objects.create(user=user, can_be_kbm=False, can_be_po=False, can_be_dp=True)
            login(request, user)
            request.session['user'] = user
            return redirect('home')
        else:
            return render(request, self.template_name, {"error": "This username is taken."})

    def get(self, request):
        return render(request, self.template_name)

class CardObject:
 pass

#Allows Product owner to create user story cards. Allows Kanban Master to create silver bullet cards.
class CreateUserStoryCard(View):
    template_name = 'create_card.html'

    def post(self, request, board_id, card_type):

        projects = utilities.getProjects(request.user, board_id, card_type)
        form = forms.CardCreationForm(request.POST, projects=projects)

        if form.is_valid():
            cleaned_form = form.cleaned_data
            card = CardObject()
            card.title = cleaned_form['title']
            card.desc = cleaned_form['description']
            card.priority = cleaned_form['priority']
            card.project = cleaned_form['project']
            card.points = cleaned_form['points']
            card.date_to_finish = cleaned_form['date_to_finish']
            card.responsible_person = cleaned_form['responsible_person']
            card.card_type = card_type

            card.first_column = Column.objects.filter(board_id=board_id).order_by('order')[0]
            card.first_subcolumn = Subcolumn.objects.filter(column=card.first_column).order_by('order')[0]

            if card_type == 'sb':
                card.priority = 'high'

            #check for wip violations
            if card.priority == 'high':
                cards = UserStoryCard.objects.filter(subcolumn=card.first_subcolumn, priority='high')
                if card.first_column.wip_limit and len(cards) > card.first_column.wip_limit:
                    request.session['card'] = card
                    return render(request, self.template_name, {'wip_violation': 1, 'board_id': board_id})

            #If no wip violation we can create card
            UserStoryCard.objects.create(title=card.title, description=card.desc, subcolumn=card.first_subcolumn,
                                         type=card_type, priority=card.priority, project=card.project,
                                         points=card.points, date_to_finish= card.date_to_finish,
                                         responsible_person=card.responsible_person)
            return render(request, self.template_name, {'card_added': 1})
        else:
            if card_type == 'sb':
                form.fields['priority'].widget.attrs['disabled'] = 'disabled'
            return render(request, self.template_name, {'form': form,  'card_type': card_type})


    def get(self, request, board_id, card_type):

        if not utilities.is_allowed_to_create_card(user=request.user, board_id=board_id, card_type=card_type):
            return render(request, "display_message.html", {"message": "You are not allowed to create this type of card."})
        projects = utilities.getProjects(request.user, board_id, card_type)

        form = forms.CardCreationForm(projects=projects)
        form.type = card_type
        if card_type == 'sb':
            form.fields['priority'].initial = 'high'
            form.fields['priority'].widget.attrs['disabled'] = 'disabled'

        return render(request, self.template_name, {'form': form, 'card_type' : card_type})


def confirm_wip_violation_and_create_card(request, board_id):
    card = request.session['card']
    added_card = UserStoryCard.objects.create(title=card.title, description=card.desc, subcolumn=card.first_subcolumn,
                                                      type=card.card_type, priority=card.priority, project=card.project,
                                                      points=card.points, date_to_finish = card.date_to_finish, responsible_person=card.responsible_person)
    WIPViolation.objects.create(card=added_card, date=datetime.datetime.now(), column=card.first_column, user=request.user, reason = 'cc')

    return render(request, 'create_card.html', {'card_added': 1})


#Function for moving card on the table
def move_card(request, board_id, card_id, direction):

    board = Board.objects.get(pk=board_id)
    card = UserStoryCard.objects.get(pk=card_id)
    subcolumn = card.subcolumn

    all_board_columns = Column.objects.filter(board=board)
    no_of_all_columns_in_board = len(all_board_columns)

    column = subcolumn.column
    all_column_subcolumns = Subcolumn.objects.filter(column=column)
    no_of_all_subcolumns_in_column = len(all_column_subcolumns)

    #Check if somehow user tries to move card which does not belong to his project
    projects_user_in = utilities.get_projects_user_in(request.user.userprofile)
    if card.project not in projects_user_in:
        return render_to_response("move_card.html", {"permission_error": "You don't have permission to move card that does not belong to your project.!"}, context_instance=RequestContext(request))

    #Check if some who is not product owner tries to move card from the acceptance testing column
    team_po = utilities.get_team_productOwner(card.project.team).user
    if request.user.id != team_po.id and subcolumn.is_acceptance_testing_column:
        return render_to_response("move_card.html", {"permission_error": "You don't have permission to move card from acceptance testing column!"}, context_instance=RequestContext(request))

    team_kbm = utilities.get_team_kanbanMaster(card.project.team).user
    developers = utilities.get_team_developers(card.project.team)

    #Move card to the left
    if direction=="left":

        #Cannot move left from left border subcolumn
        if utilities.is_left_border_subcolumn(subcolumn):
            return render_to_response("move_card.html", {"permission_error": "You cannot make that move - not allowed to move left from LB column."}, context_instance=RequestContext(request))

        if subcolumn.is_acceptance_testing_column:
            return render_to_response("move_card.html", {"permission_error": "You cannot make that move - not allowed to move left from AT column."}, context_instance=RequestContext(request))

        if utilities.is_right_of_acc_test_subcolumn(subcolumn):
            return render_to_response("move_card.html", {"permission_error": "You cannot make that move - not allowed to move left after AT column."}, context_instance=RequestContext(request))

        #There is at least one more subcoloumn to the left, move left
        if subcolumn.order > 1:
            left_subcolumn = Subcolumn.objects.get(column=column, order=subcolumn.order-1)
            #Check if there is permission for card movement
            if CardMovementPermission.objects.filter(subcolumn_from=subcolumn, subcolumn_to=left_subcolumn):
                card_movement_permission = CardMovementPermission.objects.get(subcolumn_from=subcolumn, subcolumn_to=left_subcolumn)
                #It is allowed
                if (card_movement_permission.po_allowed and request.user.id == team_po.id) or \
                    (card_movement_permission.kbm_allowed and request.user.id == team_kbm.id) or \
                    (card_movement_permission.dp_allowed and request.user.userprofile in developers):
                    card.subcolumn = left_subcolumn
                    card.save()
                    return redirect('/boards/display_board/%d' % board.id)
                #Not permitted
                else:
                    return render_to_response("move_card.html", {"permission_error": "You cannot make that move. Please contact team kanban master for more information."}, context_instance=RequestContext(request))
            card.subcolumn = left_subcolumn
            card.save()
            return redirect('/boards/display_board/%d' % board.id)

        #Move to the left column, if possible
        else:
            #Go to the left column, there is at least one
            if column.order > 1:
                left_column = Column.objects.get(board=board, order=column.order-1)
                left_column_subcolumn = Subcolumn.objects.get(column=left_column, order=len(Subcolumn.objects.filter(column=left_column)))

                #Check if there is permission for card movement
                if CardMovementPermission.objects.filter(subcolumn_from=subcolumn, subcolumn_to=left_column_subcolumn):
                    card_movement_permission = CardMovementPermission.objects.get(subcolumn_from=subcolumn, subcolumn_to=left_column_subcolumn)
                    #It is allowed
                    if (card_movement_permission.po_allowed and request.user.id == team_po.id) or \
                        (card_movement_permission.kbm_allowed and request.user.id == team_kbm.id) or \
                        (card_movement_permission.dp_allowed and request.user.userprofile in developers):
                        #Check for WIP violation - ERROR, place a warning
                        if left_column.wip_limit is not None and left_column.wip_limit <= utilities.no_of_cards_in_column(left_column):
                            return render_to_response("move_card.html", {"violation": "WIP limit violation detected! Please, confirm your action", "card": card, "board": board, "subcolumn":left_column_subcolumn}, context_instance=RequestContext(request))
                        CardMovement.objects.create(card=card, column_from=column, column_to=left_column, user=request.user)
                        card.subcolumn = left_column_subcolumn
                        card.save()
                        return redirect('/boards/display_board/%d' % board.id)
                    #Not permitted
                    else:
                        return render_to_response("move_card.html", {"permission_error": "You cannot make that move. Please contact team kanban master for more information."}, context_instance=RequestContext(request))

                #Check if WIP limit is exceeded - ERROR, place a warning
                if left_column.wip_limit is not None and left_column.wip_limit <= utilities.no_of_cards_in_column(left_column):
                    return render_to_response("move_card.html", {"violation": "WIP limit violation detected! Please, confirm your action", "card": card, "board": board, "subcolumn":left_column_subcolumn}, context_instance=RequestContext(request))

                CardMovement.objects.create(card=card, column_from=column, column_to=left_column, user=request.user)
                card.subcolumn = left_column_subcolumn
                card.save()
                return redirect('/boards/display_board/%d' % board.id)
            #This was the most left column - cannot go further left - ERROR!
            else:
                return render_to_response("move_card.html", {"message": "ERROR - cannot move left!"}, context_instance=RequestContext(request))

    #Move card to the right
    elif direction=="right":
        #There is at least one more subcoloumn to the right, move right
        if subcolumn.order < no_of_all_subcolumns_in_column:
            right_subcolumn = Subcolumn.objects.get(column=column, order=subcolumn.order+1)

            #Check if this is acceptance testing column and if all task are not completed - cannot make a move
            if right_subcolumn.is_acceptance_testing_column and not utilities.all_tasks_completed(card):
                return render_to_response("move_card.html", {"permission_error": "You cannot move card to AT column before all tasks are completed."}, context_instance=RequestContext(request))

            #Check if there is permission for card movement
            if CardMovementPermission.objects.filter(subcolumn_from=subcolumn, subcolumn_to=right_subcolumn):
                card_movement_permission = CardMovementPermission.objects.get(subcolumn_from=subcolumn, subcolumn_to=right_subcolumn)
                #It is allowed
                if (card_movement_permission.po_allowed and request.user.id == team_po.id) or \
                    (card_movement_permission.kbm_allowed and request.user.id == team_kbm.id) or \
                    (card_movement_permission.dp_allowed and request.user.userprofile in developers):
                    card.subcolumn = right_subcolumn
                    #Check if this movement starts development on the card
                    if not card.development_started and right_subcolumn.is_border_column:
                        card.development_started = True
                    card.save()
                    return redirect('/boards/display_board/%d' % board.id)
                #Not permitted
                else:
                    return render_to_response("move_card.html", {"permission_error": "You cannot make that move. Please contact team kanban master for more information."}, context_instance=RequestContext(request))
            card.subcolumn = right_subcolumn
            #Check if this movement starts development on the card
            if not card.development_started and right_subcolumn.is_border_column:
                card.development_started = True
            card.save()
            return redirect('/boards/display_board/%d' % board.id)

        #Move to the right column, if possible
        else:
            #Go to the right column, there is at least one
            if column.order < no_of_all_columns_in_board:
                right_column = Column.objects.get(board=board, order=column.order+1)
                right_column_subcolumn = Subcolumn.objects.get(column=right_column, order=1)

                #Check if this is acceptance testing column and if all task are not completed - cannot make a move
                if right_column_subcolumn.is_acceptance_testing_column and not utilities.all_tasks_completed(card):
                    return render_to_response("move_card.html", {"permission_error": "You cannot move card to AT column before all tasks are completed."}, context_instance=RequestContext(request))

                #Check if there is permission for card movement
                if CardMovementPermission.objects.filter(subcolumn_from=subcolumn, subcolumn_to=right_column_subcolumn):
                    card_movement_permission = CardMovementPermission.objects.get(subcolumn_from=subcolumn, subcolumn_to=right_column_subcolumn)
                    #It is allowed
                    if (card_movement_permission.po_allowed and request.user.id == team_po.id) or \
                        (card_movement_permission.kbm_allowed and request.user.id == team_kbm.id) or \
                        (card_movement_permission.dp_allowed and request.user.userprofile in developers):
                        #Check if WIP limit is exceeded - ERROR, place a warning
                        if right_column.wip_limit is not None and right_column.wip_limit <= utilities.no_of_cards_in_column(right_column):
                            return render_to_response("move_card.html", {"violation": "WIP limit violation detected! Please, confirm your action", "card": card, "board": board, "subcolumn": right_column_subcolumn}, context_instance=RequestContext(request))
                        CardMovement.objects.create(card=card, column_from=column, column_to=right_column, user=request.user)
                        card.subcolumn = right_column_subcolumn
                        #Check if this movement starts development on the card
                        if not card.development_started and right_column_subcolumn.is_border_column:
                            card.development_started = True
                        card.save()
                        return redirect('/boards/display_board/%d' % board.id)
                    #Not permitted
                    else:
                        return render_to_response("move_card.html", {"permission_error": "You cannot make that move. Please contact team kanban master for more information."}, context_instance=RequestContext(request))
                #Check if WIP limit is exceeded - ERROR, place a warning
                if right_column.wip_limit is not None and right_column.wip_limit <= utilities.no_of_cards_in_column(right_column):
                    return render_to_response("move_card.html", {"violation": "WIP limit violation detected! Please, confirm your action", "card": card, "board": board, "subcolumn": right_column_subcolumn}, context_instance=RequestContext(request))
                CardMovement.objects.create(card=card, column_from=column, column_to=right_column, user=request.user)
                card.subcolumn = right_column_subcolumn
                #Check if this movement starts development on the card
                if not card.development_started and right_column_subcolumn.is_border_column:
                    card.development_started = True
                card.save()
                return redirect('/boards/display_board/%d' % board.id)
            #This was the most right column - cannot go further right - ERROR!
            else:
                return render_to_response("move_card.html", {"message": "ERROR - cannot move right!"}, context_instance=RequestContext(request))

    #Move card to custum sub-column - product owner has declined story


#Shows all movement permissiona for card to be moved from one subcolumn to another
def card_movement_permission(request, board_id):
    board = Board.objects.get(pk=board_id)
    subcolumns = utilities.get_board_subcolumns(board)
    #Get all subcolumns
    card_movement_permissions = []
    for subcolumn in subcolumns:
        card_movement_permissions += CardMovementPermission.objects.filter(subcolumn_from=subcolumn)

    first_swim_lane = SwimLane.objects.filter(board=board)[0]
    team_kbm = utilities.get_team_kanbanMaster(first_swim_lane.project.team).user
    return render_to_response("card_movement_permission.html", {"board":board, "card_movement_permissions":card_movement_permissions, "team_kbm":team_kbm}, context_instance=RequestContext(request))


#For kanban master to be able to add card movement permission
def add_card_movement_permission(request, board_id):
    board = Board.objects.get(pk=board_id)

    if request.method == 'POST':
        create_card_movement_permission_form = forms.CreateCardMovementPermission(request.POST, board=board)
        #Form valid - create permission
        if create_card_movement_permission_form.is_valid():
            subcolumn_from_id = create_card_movement_permission_form.cleaned_data['subcolumn_from']
            subcolumn_to_id = create_card_movement_permission_form.cleaned_data['subcolumn_to']
            po_allowed = create_card_movement_permission_form.cleaned_data['po_allowed']
            kbm_allowed = create_card_movement_permission_form.cleaned_data['kbm_allowed']
            dp_allowed = create_card_movement_permission_form.cleaned_data['dp_allowed']
            subcolumn_from = Subcolumn.objects.get(pk=subcolumn_from_id)
            subcolumn_to = Subcolumn.objects.get(pk=subcolumn_to_id)
            CardMovementPermission.objects.create(subcolumn_from=subcolumn_from, subcolumn_to=subcolumn_to, po_allowed=po_allowed, kbm_allowed=kbm_allowed, dp_allowed=dp_allowed)
            return render_to_response("add_card_movement_permission.html", {"create_card_movement_permission_form":create_card_movement_permission_form, "success":"Permission added!"}, context_instance=RequestContext(request))
        #Form not valid
        else:
            return render_to_response("add_card_movement_permission.html", {"create_card_movement_permission_form":create_card_movement_permission_form}, context_instance=RequestContext(request))

    else:
        first_swim_lane = SwimLane.objects.filter(board=board)[0]
        team_kbm = utilities.get_team_kanbanMaster(first_swim_lane.project.team)
        #User is team kanban master
        if team_kbm.user.id == request.user.id:
            create_card_movement_permission_form = forms.CreateCardMovementPermission(board=board)
            return render_to_response("add_card_movement_permission.html", {"create_card_movement_permission_form":create_card_movement_permission_form}, context_instance=RequestContext(request))
        #User has no right to create permission
        else:
            return render_to_response("add_card_movement_permission.html", {"permission_error":"You dont have permission to create card movement permission"}, context_instance=RequestContext(request))


#For kanban master to be able to edit card movement permission
def edit_card_movement_permission(request, board_id, card_movement_permission_id):
    board = Board.objects.get(pk=board_id)
    card_movement_permission = CardMovementPermission.objects.get(pk=card_movement_permission_id)

    if request.method == 'POST':
        edit_card_movement_permission_form = forms.EditCardMovementPermission(request.POST, board=board)
        #Valid form - only roles can be updated, subcolumns no
        if edit_card_movement_permission_form.is_valid():
            #Extract data
            po_allowed = edit_card_movement_permission_form.cleaned_data['po_allowed']
            kbm_allowed = edit_card_movement_permission_form.cleaned_data['kbm_allowed']
            dp_allowed = edit_card_movement_permission_form.cleaned_data['dp_allowed']
            #Update permission
            card_movement_permission.po_allowed = po_allowed
            card_movement_permission.kbm_allowed = kbm_allowed
            card_movement_permission.dp_allowed = dp_allowed
            card_movement_permission.save()
            return render_to_response("edit_card_movement_permission.html", {"edit_card_movement_permission_form":edit_card_movement_permission_form, "success":"Permission edited!"}, context_instance=RequestContext(request))
        #Form not valid
        else:
            return render_to_response("edit_card_movement_permission.html", {"edit_card_movement_permission_form":edit_card_movement_permission_form}, context_instance=RequestContext(request))

    else:
        first_swim_lane = SwimLane.objects.filter(board=board)[0]
        team_kbm = utilities.get_team_kanbanMaster(first_swim_lane.project.team)
        #User is team kanban master
        if team_kbm.user.id == request.user.id:
            edit_card_movement_permission_form = forms.EditCardMovementPermission(board=board, initial={'subcolumn_from':card_movement_permission.subcolumn_from.column.name + " -> " + card_movement_permission.subcolumn_from.name, 'subcolumn_to':card_movement_permission.subcolumn_to.column.name + " -> " + card_movement_permission.subcolumn_to.name, 'po_allowed':card_movement_permission.po_allowed, 'kbm_allowed':card_movement_permission.kbm_allowed, 'dp_allowed':card_movement_permission.dp_allowed})
            return render_to_response("edit_card_movement_permission.html", {"edit_card_movement_permission_form":edit_card_movement_permission_form}, context_instance=RequestContext(request))
        #User has no right to edit permission
        else:
            return render_to_response("edit_card_movement_permission.html", {"permission_error":"You dont have permission to create card movement permission"}, context_instance=RequestContext(request))


#For kanban master to be able to remove card movement permission
def remove_card_movement_permission(request, board_id, card_movement_permission_id):
    board = Board.objects.get(pk=board_id)
    first_swim_lane = SwimLane.objects.filter(board=board)[0]
    team_kbm = utilities.get_team_kanbanMaster(first_swim_lane.project.team)
    #Delete if user is team kanban master
    if team_kbm.user.id == request.user.id:
        CardMovementPermission.objects.get(pk=card_movement_permission_id).delete()
        return redirect('/boards/display_board/%d/card_movement_permission' % board.id)
    #User has no right to delete permission
    else:
        return HttpResponse('<h1>You do not have permission for this action.</h1>')


#User has confiremed violation of WIP
def confirm_wip_violation(request, board_id, card_id, subcolumn_id):
    card = UserStoryCard.objects.get(pk=card_id)
    subcolumn_to = Subcolumn.objects.get(pk=subcolumn_id)
    WIPViolation.objects.create(card=card, column=subcolumn_to.column, user=request.user, date=datetime.date.today(), reason='cm')
    CardMovement.objects.create(card=card, column_from=card.subcolumn.column, column_to=subcolumn_to.column, user=request.user)
    card.subcolumn = subcolumn_to
    #Check if this movement starts development on the card
    if not card.development_started and subcolumn_to.is_border_column:
        card.development_started = True
    card.save()

    return redirect('/boards/display_board/%c' % board_id)


#Product owner can decline story and move the card to the highest priority column or and column left of it
def decline_story(request, board_id, card_id):
    board = Board.objects.get(pk=board_id)
    card = UserStoryCard.objects.get(pk=card_id)
    subcolumn_from = card.subcolumn
    column_from = subcolumn_from.column

    #Check if user is not product owner of the team - cannot decline
    team_po = utilities.get_team_productOwner(card.project.team)
    if team_po.user.id != request.user.id:
        return render_to_response("decline_story.html", {"permission_error": "Only product owner can decline story!"}, context_instance=RequestContext(request))


    if request.method == 'POST':
        subcolumn_declined_story_form = forms.SubcolumnDeclinedStory(request.POST, board=board)
        if subcolumn_declined_story_form.is_valid():
            subcolumn_to_id = subcolumn_declined_story_form.cleaned_data['subcolumn']
            subcolumn_to = Subcolumn.objects.get(pk=subcolumn_to_id)

            if subcolumn_to.column.id == subcolumn_from.column.id:
                card.subcolumn = subcolumn_to
                card.type = "rc"
                card.save()
                return redirect('/boards/display_board/%d' % board.id)

            else:
                column_to = subcolumn_to.column
                if column_to.wip_limit is not None and column_to.wip_limit <= utilities.no_of_cards_in_column(column_to):
                    return render_to_response("move_card.html", {"violation": "WIP limit violation detected! Please, confirm your action", "card": card, "board": board, "subcolumn":subcolumn_to}, context_instance=RequestContext(request))

                CardMovement.objects.create(card=card, column_from=column_from, column_to=column_to, user=request.user)
                card.subcolumn = subcolumn_to
                card.type = "rc"
                card.save()
                return redirect('/boards/display_board/%d' % board.id)


    else:
        subcolumn_declined_story_form = forms.SubcolumnDeclinedStory(board=board)
        return render_to_response("decline_story.html", {"subcolumn_declined_story_form": subcolumn_declined_story_form}, context_instance=RequestContext(request))


#Function for display of card details
def card_details(request, card_id):
    card = UserStoryCard.objects.get(pk=card_id)
    card_movements = CardMovement.objects.filter(card=card)
    card_wip_violations = WIPViolation.objects.filter(card=card)
    tasks = Task.objects.filter(card=card)
    return render_to_response("card_details.html", {"card": card, "card_movements": card_movements, "card_wip_violations": card_wip_violations, "tasks":tasks}, context_instance=RequestContext(request))


#Function for deleting card
def delete_card(request, card_id):
    card = UserStoryCard.objects.get(pk=card_id)

    if request.method == 'POST':
        delete_card_form = forms.DeleteCardForm(request.POST)
        if delete_card_form.is_valid():
            reason = delete_card_form.cleaned_data['reason']
            card.deleted = True
            card.deletion_reason = reason
            card.save()
            return render_to_response("delete_card.html", {"message":"Card successfully deleted!"}, context_instance=RequestContext(request))
        else:
            return render_to_response("delete_card.html", {"delete_card_form": delete_card_form}, context_instance=RequestContext(request))

    else:
        team_po = utilities.get_team_productOwner(card.project.team)
        team_kbm = utilities.get_team_kanbanMaster(card.project.team)
        delete_card_form = forms.DeleteCardForm();
        #User is kanban master - can delete card
        if team_kbm.user.id == request.user.id:
            return render_to_response("delete_card.html", {"delete_card_form": delete_card_form}, context_instance=RequestContext(request))

        #User is product owner - can delete card where development has not yet started
        elif team_po.user.id == request.user.id:
            if card.development_started:
                return render_to_response("delete_card.html", {"message":"Cannot delete card. Development has already started!"}, context_instance=RequestContext(request))
            else:
                return render_to_response("delete_card.html", {"delete_card_form": delete_card_form}, context_instance=RequestContext(request))

        #User is developer - cannot delete card
        else:
            return render_to_response("delete_card.html", {"message":"You dont have permission to delete card!"}, context_instance=RequestContext(request))

#Function for adding task to the card
def add_task(request, card_id):
    card = UserStoryCard.objects.get(pk=card_id)
    tasks = Task.objects.filter(card=card)

    #Check for permission - only developers can add tasks
    if request.user.userprofile not in utilities.get_team_developers(card.project.team):
        return HttpResponse('<h1>You do not have permission to see this page.</h1>')

    if request.method == 'POST':
        create_task_form = forms.CreateTask(request.POST)
        if create_task_form.is_valid():
            description = create_task_form.cleaned_data['description']
            hours = create_task_form.cleaned_data['hours']
            Task.objects.create(description=description, hours=hours, card=card)
            create_task_form = forms.CreateTask()
            return render_to_response("add_tasks.html", {"create_task_form":create_task_form, "tasks": tasks}, context_instance=RequestContext(request))
        else:
            return render_to_response("add_tasks.html", {"create_task_form":create_task_form, "tasks": tasks}, context_instance=RequestContext(request))

    else:
        create_task_form = forms.CreateTask()
        return render_to_response("add_tasks.html", {'create_task_form':create_task_form, "tasks": tasks}, context_instance=RequestContext(request))

#Function for editing task
def edit_task(request, card_id, task_id):
    task = Task.objects.get(pk=task_id)
    card = UserStoryCard.objects.get(pk=card_id)
    tasks = Task.objects.filter(card=card)

    #Check for permission - only developers can edit tasks
    if request.user.userprofile not in utilities.get_team_developers(card.project.team):
        return HttpResponse('<h1>You do not have permission to see this page.</h1>')

    if request.method == 'POST':
        edit_task_form = forms.EditTask(request.POST)
        if edit_task_form.is_valid():
            description = edit_task_form.cleaned_data['description']
            hours = edit_task_form.cleaned_data['hours']
            completed = edit_task_form.cleaned_data['completed']
            task.description = description
            task.hours = hours
            task.completed = completed
            task.save()
            return redirect('/cards/%d/card_details/' % card.id)
        else:
            return render_to_response("edit_task.html", {"edit_task_form":edit_task_form, "tasks": tasks}, context_instance=RequestContext(request))

    else:
        edit_task_form = forms.EditTask(initial={'description':task.description, 'hours':task.hours, 'completed':task.completed})
        #edit_task_form.description = task.description
        #edit_task_form.size = task.size
        #edit_task_form.completed = task.completed
        return render_to_response("edit_task.html", {"edit_task_form":edit_task_form, "tasks": tasks}, context_instance=RequestContext(request))


#Function for deleting task
def delete_task(request, card_id, task_id, to_redir):
    Task.objects.get(pk=task_id).delete()
    card = UserStoryCard.objects.get(pk=card_id)

    #Check for permission - only developers can delete tasks
    if request.user.userprofile not in utilities.get_team_developers(card.project.team):
        return HttpResponse('<h1>You do not have permission to see this page.</h1>')

    if to_redir == "1":
        return redirect('/cards/%d/card_details/' % card.id)
    elif to_redir == "2":
        return redirect('/cards/%d/add_task/' % card.id)

    #return render_to_response("edit_task.html", context_instance=RequestContext(request))


class EditCard(View):
    template_name = 'edit_card.html'

    def post(self, request, card_id):
        card = UserStoryCard.objects.get(pk=card_id)
        projects = utilities.get_all_projects(request.user, board_id=card.subcolumn.column.board.pk)
        form = forms.CardEditForm(request.POST, projects=projects, card=card_id)
        user_profile = UserProfile.objects.filter(user=request.user)

        #uporabnik lahko posodablja samo kartice, ki pripadajo njegovemu projektu
        if not utilities.card_in_your_project(card=card, user_profile=user_profile):
            message = "Card is not in your project."
            return render(request, self.template_name, {'message': message, 'board_id' : card.subcolumn.column.board.pk})

        if form.is_valid():
            cleaned_form = form.cleaned_data

            card.title = cleaned_form['title']
            card.description = cleaned_form['description']
            card.priority = cleaned_form['priority']
            card.project = cleaned_form['project']
            card.points = cleaned_form['points']
            card.date_to_finish = cleaned_form['date_to_finish']
            card.responsible_person = cleaned_form['responsible_person']
            card.save()

            if card.type == 'sb':
                card.priority = 'high'
            message = "Your card has been successfuly modified! "
            return render(request, self.template_name, {'message': message, 'board_id' : card.subcolumn.column.board.pk})
        else:
            return render(request, self.template_name,{'form' : form})

    def get(self, request, card_id):
        card = UserStoryCard.objects.get(pk=card_id)
        user_profile = UserProfile.objects.filter(user=request.user)
        #uporabnik lahko posodablja samo kartice, ki pripadajo njegovemu projektu
        if not utilities.card_in_your_project(card=card, user_profile=user_profile):
            message = "Card is not in your project."
            return render(request, self.template_name, {'message': message, 'board_id' : card.subcolumn.column.board.pk})

        #posodabljanje pred vzstopom v zacenti mejni stolpec
        card_position = utilities.card_allowed_to_edit(card)
        team1 = card.project.team
        if card_position==-2:
            message = "Board has to contain two border columns"
            return render(request, self.template_name, {'message': message, 'board_id' : card.subcolumn.column.board.pk})
        if card_position==-1:
            if not (request.user.userprofile == utilities.get_team_kanbanMaster(team1) or
                    request.user.userprofile == utilities.get_team_productOwner(team1)):
                message = "Only Product owner and Kanban master can edit card left of borders."
                return render(request, self.template_name, {'message': message, 'board_id': card.subcolumn.column.board.pk})
        if card_position == 1:
            message = "It is not allowed to edit cards that are right of the border."
            return render(request, self.template_name, {'message': message, 'board_id': card.subcolumn.column.board.pk})

        developers = utilities.get_team_developers(team1)
        is_developer = 0
        if request.user.userprofile in developers:
            is_developer = 1

        if card_position == 0 and not (is_developer or request.user.userprofile == utilities.get_team_kanbanMaster(team1)):
            message = "Only developer and Kanban master are allowed to edit inside borders."
            return render(request, self.template_name, {'message': message, 'board_id': card.subcolumn.column.board.pk})
        projects = utilities.get_all_projects(request.user, board_id=card.subcolumn.column.board.pk)
        form = forms.CardEditForm(projects=projects, card=card_id)
        return render(request, 'edit_card.html', {'form': form})

#Showing all teams user is in
def team(request):
    user_profile = UserProfile.objects.filter(user=request.user)
    #teams = Team.objects.filter(members__in=user_profile, members__memberteam__is_active=True) - ne dela pravilno, ne vem zakaj
    teams = utilities.get_teams_user_in(user_profile, True)
    return render_to_response("teams/team.html", {"teams": teams}, context_instance=RequestContext(request))


#Create team - kanban master can create teams
def create_team(request):
    temp_user = request.user
    user = temp_user.userprofile

    #Form has been submitted
    if request.method == 'POST':
        create_team_form = forms.CreateTeamForm(request.POST)

        #Form is valid save all the data into database
        if create_team_form.is_valid():
            team_name= create_team_form.cleaned_data['team_name']
            po_id = create_team_form.cleaned_data['product_owner']
            kbm_id = create_team_form.cleaned_data['kanban_master']
            developers_ids = create_team_form.cleaned_data['developers']

            kbm_role = Role.objects.get(role_name="KanbanMaster")
            po_role = Role.objects.get(role_name="ProductOwner")
            dp_role = Role.objects.get(role_name="Developer")

            team_member_ids = set(set([po_id]) | set([kbm_id]) | set(developers_ids))

            #Creating new team
            team = Team.objects.create(team_name=team_name)
            team.save()

            #Save info about each member being part of this team
            for team_member_id in team_member_ids:
                member = UserProfile.objects.get(pk=team_member_id)
                member_team = MemberTeam.objects.create(member=member, team=team, is_active=True)
                member_team.save()

                #All members in team are active at the beginning
                member_team_activity = MemberTeamActivity.objects.create(member_team=member_team, active_from=timezone.now())
                member_team_activity.save()

                #Save roles which are being given to each user
                if team_member_id==po_id:
                    member_team_role_1 = MemberTeamRole.objects.create(member_team=member_team, role=po_role)
                    member_team_role_1.save()

                if team_member_id==kbm_id:
                    member_team_role_2 = MemberTeamRole.objects.create(member_team=member_team, role=kbm_role)
                    member_team_role_2.save()

                if team_member_id in developers_ids:
                    member_team_role_3 = MemberTeamRole(member_team=member_team, role=dp_role)
                    member_team_role_3.save()



            return render_to_response("teams/create_team.html", context_instance=RequestContext(request))

        #Form not valid, repeat
        else:
            return render_to_response("teams/create_team.html", {"create_team_form":create_team_form}, context_instance=RequestContext(request))

    #First time visiting
    else:
        #If user has permission to create teams
        if not user.can_be_kbm:
            return render_to_response("teams/create_team.html", {"permission_error": "You don't have permission to create teams"}, context_instance=RequestContext(request))
        else:
            create_team_form = forms.CreateTeamForm()
            return render_to_response("teams/create_team.html", {"create_team_form":create_team_form}, context_instance=RequestContext(request))


#Showing team members
def show_team(request, team_id):
    team = Team.objects.get(pk=team_id)
    developers = []
    kbm = None
    po = None
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)

    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "KanbanMaster":
                kbm = team_member.member
            elif team_member_role.role.role_name == "ProductOwner":
                po = team_member.member
            elif team_member_role.role.role_name == "Developer":
                developers.append(team_member.member)

    return render_to_response("teams/show_team.html", {"team":team, "developers":developers, "kbm":kbm, "po":po}, context_instance=RequestContext(request))


#Kanban master can update team
def update_team(request, team_id):
    temp_user = request.user
    user = temp_user.userprofile
    team = Team.objects.get(pk=team_id)
    kanban_master = utilities.get_team_kanbanMaster(team)
    product_owner = utilities.get_team_productOwner(team)

    if request.method=='POST':
        update_team_form = forms.UpdateTeamForm(request.POST, team=team)

        if update_team_form.is_valid():

            #Obtain data from update team form
            update_kbm_id = int(update_team_form.cleaned_data['kanban_master'])
            update_po_id = int(update_team_form.cleaned_data['product_owner'])
            add_developers_ids = update_team_form.cleaned_data['add_developers']
            remove_developers_ids = update_team_form.cleaned_data['remove_developers']

            #Obtain roles data
            kbm_role = Role.objects.get(role_name="KanbanMaster")
            po_role = Role.objects.get(role_name="ProductOwner")
            dp_role = Role.objects.get(role_name="Developer")

            ########### ADDING DATA ###############
            #Check if new kanban master is selected
            if not kanban_master.id==update_kbm_id:
                update_kbm = UserProfile.objects.get(pk=update_kbm_id)

                #Already is/was part of the team
                if MemberTeam.objects.filter(member=update_kbm, team=team):
                    mt_update_kbm = MemberTeam.objects.get(member=update_kbm, team=team)
                    MemberTeamRole.objects.create(member_team=mt_update_kbm, role=kbm_role)
                    if not mt_update_kbm.is_active:
                        mt_update_kbm.is_active=True
                        mt_update_kbm.save()
                        MemberTeamActivity.objects.create(member_team=mt_update_kbm, active_from=timezone.now())
                #First time joining the team
                else:
                    mt_update_kbm = MemberTeam(member=update_kbm, team=team, is_active=True)
                    mt_update_kbm.save()
                    MemberTeamRole.objects.create(member_team=mt_update_kbm, role=kbm_role)
                    MemberTeamActivity.objects.create(member_team=mt_update_kbm, active_from=timezone.now())

            #Check if new product owner is selected:
            if not product_owner.id==update_po_id:
                update_po = UserProfile.objects.get(pk=update_po_id)

                #Already is/was part of the team
                if MemberTeam.objects.filter(member=update_po, team=team):
                    mt_update_po = MemberTeam.objects.get(member=update_po, team=team)
                    MemberTeamRole.objects.create(member_team=mt_update_po, role=po_role)
                    if not mt_update_po.is_active:
                        mt_update_po.is_active=True
                        mt_update_po.save()
                        MemberTeamActivity.objects.create(member_team=mt_update_po, active_from=timezone.now())
                #First time joining the team
                else:
                    mt_update_po = MemberTeam(member=update_po, team=team, is_active=True)
                    mt_update_po.save()
                    MemberTeamRole.objects.create(member_team=mt_update_po, role=po_role)
                    MemberTeamActivity.objects.create(member_team=mt_update_po, active_from=timezone.now())

            #Adding new developers
            for add_developer_id in add_developers_ids:
                new_developer = UserProfile.objects.get(pk=add_developer_id)

                #Already is/was part of the team
                if MemberTeam.objects.filter(member=new_developer, team=team):
                    mt_new_developer = MemberTeam.objects.get(member=new_developer, team=team)
                    MemberTeamRole.objects.create(member_team=mt_new_developer, role=dp_role)
                    if not mt_new_developer.is_active:
                        mt_new_developer.is_active=True
                        mt_new_developer.save()
                        MemberTeamActivity.objects.create(member_team=mt_new_developer, active_from=timezone.now())
                #First time joining the team
                else:
                    mt_new_developer = MemberTeam(member=new_developer, team=team, is_active=True)
                    mt_new_developer.save()
                    MemberTeamRole.objects.create(member_team=mt_new_developer, role=dp_role)
                    MemberTeamActivity.objects.create(member_team=mt_new_developer, active_from=timezone.now())


            ############# DELETING DATA ########################
            #Changes only if new kanban master is selected
            if not kanban_master.id==update_kbm_id:
                mt_kbm = MemberTeam.objects.get(member=kanban_master, team=team)
                MemberTeamRole.objects.get(member_team=mt_kbm, role=kbm_role).delete()
                #If it has no other role, put his status on inactive!
                if not MemberTeamRole.objects.filter(member_team=mt_kbm):
                    mt_kbm.is_active=False
                    mt_kbm.save()
                    mt_activity = MemberTeamActivity.objects.get(member_team=mt_kbm, active_until__isnull=True)
                    mt_activity.active_until=timezone.now()
                    mt_activity.save()

            #Changes only if new product owner is selected
            if not product_owner.id==update_po_id:
                mt_po = MemberTeam.objects.get(member=product_owner, team=team)
                MemberTeamRole.objects.get(member_team=mt_po, role=po_role).delete()
                #If it has no other role, put his status on inactive!
                if not MemberTeamRole.objects.filter(member_team=mt_po):
                    mt_po.is_active=False
                    mt_po.save()
                    mt_activity = MemberTeamActivity.objects.get(member_team=mt_po, active_until__isnull=True)
                    mt_activity.active_until=timezone.now()
                    mt_activity.save()

            #Removing developers...
            for remove_developer_id in remove_developers_ids:
                remove_developer = UserProfile.objects.get(pk=remove_developer_id)
                mt_remove_developer = MemberTeam.objects.get(member=remove_developer, team=team)
                MemberTeamRole.objects.get(member_team=mt_remove_developer, role=dp_role).delete()
                #If it has no other role, put his status on inactive!
                if not MemberTeamRole.objects.filter(member_team=mt_remove_developer):
                    mt_remove_developer.is_active=False
                    mt_remove_developer.save()
                    mt_activity = MemberTeamActivity.objects.get(member_team=mt_remove_developer, active_until__isnull=True)
                    mt_activity.active_until=timezone.now()
                    mt_activity.save()
            return render_to_response("teams/update_team.html", context_instance=RequestContext(request))
        #Form is not valid - repeat...
        else:
            return render_to_response("teams/update_team.html", {"update_team_form":update_team_form, "team": team}, context_instance=RequestContext(request))

    else:
        if not user.id==kanban_master.id:
            return render_to_response("teams/update_team.html", {"permission_error":"Only team kanban master can update teams"}, context_instance=RequestContext(request))
        else:
            team = Team.objects.get(pk=team_id)
            update_team_form = forms.UpdateTeamForm(team=team)
            return render_to_response("teams/update_team.html", {"update_team_form":update_team_form, "team":team}, context_instance=RequestContext(request))


#It shows all of activities of member in all the teams he is/was involved
def team_activity(request):
    user = request.user.userprofile
    teams = utilities.get_teams_user_in(user, False) + utilities.get_teams_user_in(user, True)
    all_teams_activities = []
    for team in teams:
        member_team = MemberTeam.objects.get(member=user, team=team)
        member_team_activities = MemberTeamActivity.objects.filter(member_team=member_team)
        all_teams_activities.append((team, member_team_activities))

    #team = Team.objects.get(pk=team_id)
    #member_team = MemberTeam.objects.get(member=user, team=team)
    #member_team_activities = MemberTeamActivity.objects.filter(member_team=member_team)
    return render_to_response("teams/activity.html", {"all_teams_activities":all_teams_activities}, context_instance=RequestContext(request))


#All projects user is involved into
def project(request):
    user = request.user.userprofile
    projects = [(project, utilities.get_team_kanbanMaster(project.team)) for project in utilities.get_projects_user_in(user)]
    return render_to_response("projects/project.html", {"projects": projects}, context_instance=RequestContext(request))


#Kanban master can create new project
def create_project(request):
    user = request.user.userprofile

    if request.method=='POST':
        create_project_form = forms.CreateProjectForm(request.POST, user_kbm=user)
        if create_project_form.is_valid():
            code = create_project_form.cleaned_data['code']
            title = create_project_form.cleaned_data['title']
            client_name = create_project_form.cleaned_data['client_name']
            start_date = create_project_form.cleaned_data['start_date']
            anticipated_end_date = create_project_form.cleaned_data['anticipated_end_date']
            team_id = create_project_form.cleaned_data['team']

            team = Team.objects.get(pk=team_id)
            Project.objects.create(code=code, title=title, client_name=client_name, start_date=start_date, anticipated_end_date=anticipated_end_date, team=team, is_active=True, work_started=False)

            return render_to_response("projects/create_project.html", context_instance=RequestContext(request))
        #Form not valid, repeat
        else:
            return render_to_response("projects/create_project.html", {"create_project_form":create_project_form}, context_instance=RequestContext(request))

    else:
         #If user has permission to create teams
        if not user.can_be_kbm:
            return render_to_response("projects/create_project.html", {"permission_error": "You dont have permission to create projects"}, context_instance=RequestContext(request))
        else:
            create_project_form = forms.CreateProjectForm(user_kbm=user)
            return render_to_response("projects/create_project.html", {"create_project_form":create_project_form}, context_instance=RequestContext(request))

    return render_to_response("projects/create_project.html", context_instance=RequestContext(request))


#Kanban master can edit projects
def edit_project(request, project_id):
    user = request.user.userprofile
    project = Project.objects.get(pk=project_id)

    if request.method=='POST':
        edit_project_form = forms.EditProjectForm(request.POST, user_kbm=user, project=project)
        if edit_project_form.is_valid():

            project.title = edit_project_form.cleaned_data['title']
            project.client_name = edit_project_form.cleaned_data['client_name']
            project.start_date = edit_project_form.cleaned_data['start_date']
            project.anticipated_end_date = edit_project_form.cleaned_data['anticipated_end_date']
            if edit_project_form.cleaned_data['finished_date']:
                project.finished_date = edit_project_form.cleaned_data['finished_date']
            project.team = Team.objects.get(pk=edit_project_form.cleaned_data['team'])
            project.save()

            return render_to_response("projects/edit_project.html", context_instance=RequestContext(request))

        else:
            return render_to_response("projects/edit_project.html", {"edit_project_form": edit_project_form, "project": project}, context_instance=RequestContext(request))
    else:
        if not user.id == utilities.get_team_kanbanMaster(project.team).id:
            return render_to_response("projects/edit_project.html", {"permission_error": "You dont have permission to edit this project"}, context_instance=RequestContext(request))
        else:
            edit_project_form = forms.EditProjectForm(user_kbm=user, project=project)
            return render_to_response("projects/edit_project.html", {"edit_project_form": edit_project_form, "project": project}, context_instance=RequestContext(request))


#Kanban master can deactivate project - only if work has not yet started
def deactivate_project(request, project_id):
    user = request.user.userprofile
    project = Project.objects.get(pk=project_id)

    if project.work_started:
        projects = [(project, utilities.get_team_kanbanMaster(project.team)) for project in utilities.get_projects_user_in(user)]
        return render_to_response("projects/project.html", {"projects": projects, 'deactivation_error': "Unable to deactivate project - work has alread started!"}, context_instance=RequestContext(request))

    else:
        project.is_active = False
        project.save()
        projects = [(project, utilities.get_team_kanbanMaster(project.team)) for project in utilities.get_projects_user_in(user)]
        return render_to_response("projects/project.html", {"projects": projects}, context_instance=RequestContext(request))


#Kanban master can activate project
def activate_project(request, project_id):
    user = request.user.userprofile
    project = Project.objects.get(pk=project_id)

    project.is_active = True
    project.save()
    projects = [(project, utilities.get_team_kanbanMaster(project.team)) for project in utilities.get_projects_user_in(user)]
    return render_to_response("projects/project.html", {"projects": projects}, context_instance=RequestContext(request))

class DisplayBoard(View):
    template_name = 'boards/display_board.html'

    def get(self, request, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=request.user)
        boards = utilities.get_boards_user_in(user_profile)
        board = Board.objects.get(pk=int(kwargs['board_id']))

        if (not boards or not board in boards) and not user_profile.user.is_superuser:
            return render(request, "display_message.html", {"message": "You can not access this board."})

        columns = Column.objects.filter(board=board.pk).order_by('order')

        columns_width = [(column.subcolumn_set.all().count()*200, column)for column in columns]

        swim_lanes = SwimLane.objects.filter(board=board.pk).order_by('order')

        #cards = UserStoryCard.objects.filter(column__in=columns)

        #user_with_roles = getRoles(request.user, board)

        no_columns = False
        if columns.count() == 0:
            no_columns = True

        no_swim_lanes = False
        if swim_lanes.count() == 0:
            no_swim_lanes = True

        editable = False
        if user_profile.can_be_kbm and not utilities.get_cards_in_board(board).exists():
            editable = True

        is_kbm = False
        if utilities.get_cards_in_board(board) == 0 \
                or swim_lanes.count() == 0 \
                or utilities.get_team_kanbanMaster(swim_lanes[0].project.team) == user_profile:
            is_kbm = True

        columns_consistent = False
        if utilities.are_board_columns_properties_consistent(board):
            columns_consistent = True

        return render(request, self.template_name, {'board': board,
                                                    'columns': columns_width,
                                                    'swim_lanes': swim_lanes,
                                                    #'cards': cards,
                                                    #'user_with_roles' : user_with_roles,
                                                    'no_columns': no_columns,
                                                    'no_swim_lanes': no_swim_lanes,
                                                    'editable': editable,
                                                    'is_kbm': is_kbm,
                                                    'columns_consistent': columns_consistent,
                                                    'curr_date': datetime.datetime.now(),
        })


class BoardsList(View):
    template_name = 'boards/boards_list.html'

    def get(self, request):
        user_profile = UserProfile.objects.get(user=request.user)
        boards = []
        if (user_profile.user.is_superuser):
            boards = Board.objects.all()
        else:
            boards = utilities.get_boards_user_in(user_profile)
        return render(request, self.template_name, {"boards": boards})

class CreateBoard(View):
    template_name = 'boards/create_board.html'

    def get(self, request):
        user_profile = request.user.userprofile
        # Only kanban master can create boards.
        if user_profile.can_be_kbm:
            create_board_form = forms.CreateBoardForm()
            return render(request, self.template_name, {"create_board_form": create_board_form,
                                                        "title": "Create empty board"})
        else:
            return render(request, self.template_name, {"permission_error": "You don't have permissions to create boards"})

    def post(self, request):
        create_board_form = forms.CreateBoardForm(request.POST)
        if create_board_form.is_valid():
            board_name = create_board_form.cleaned_data['board_name']

            board = Board.objects.create(name=board_name)
            board.save()

            return render(request, self.template_name, {"message": "Board successfully created!"})
        else:
            return render(request, self.template_name, {"permission_error": "TODO: change this"})

def copy_board_structure(request, board_id):
    template_name = 'boards/create_board.html'

    old_board = get_object_or_404(Board, pk=board_id)

    create_board_form = forms.CreateBoardForm(request.POST or None)
    if create_board_form.is_valid():
        new_board_name = create_board_form.cleaned_data['board_name']

        new_board = Board.objects.create(name=new_board_name)
        new_board.save()

        for old_column in Column.objects.filter(board=old_board):
            new_column = Column.objects.create(board=new_board, order=old_column.order, name=old_column.name,
                                               wip_limit=old_column.wip_limit);
            new_column.save()

            for old_subcolumn in Subcolumn.objects.filter(column=old_column):
                new_subcolumn = Subcolumn.objects.create(column=new_column, order=old_subcolumn.order,
                                         name=old_subcolumn.name,
                                         is_border_column=old_subcolumn.is_border_column,
                                         is_highest_priority_card_column=old_subcolumn.is_highest_priority_card_column,
                                         is_acceptance_testing_column=old_subcolumn.is_acceptance_testing_column)
                new_subcolumn.save()

        return render(request, template_name, {"message": "Beard successfully copied.",
                                           "title": "Copy board structure"})
    else:
        return render(request, template_name, {"create_board_form": create_board_form,
                                               "title": "Copy board structure"})

class CreateColumn(View):
    template_name = 'boards/create_column.html'

    def get(self, request, *args, **kwargs):
        board = Board.objects.get(pk=int(kwargs['board_id']))
        user_profile = request.user.userprofile
        # Only kanban master can create columns.
        if user_profile.can_be_kbm:
            create_column_form = forms.CreateColumnForm()
            return render(request, self.template_name, {"create_column_form": create_column_form,
                                                        "board": board,
                                                        "order": int(kwargs['order']),
                                                        })
        else:
            return render(request, self.template_name, {"permission_error": "You don't have permissions to create columns"})

    def post(self, request, *args, **kwargs):
        board = Board.objects.get(pk=int(kwargs['board_id']))
        create_column_form = forms.CreateColumnForm(request.POST)

        if create_column_form.is_valid():
            name = create_column_form.cleaned_data['name']
            wip_limit = create_column_form.cleaned_data['wip_limit']
            is_border_column = create_column_form.cleaned_data['is_border_column']
            is_highest_priority_card_column = create_column_form.cleaned_data['is_highest_priority_card_column']
            is_acceptance_testing_column = create_column_form.cleaned_data['is_acceptance_testing_column']

            order = int(kwargs['order'])
            if Column.objects.filter(order=order).exists():
                Column.objects.filter(order__gte=order).update(order=F('order') + 1)

            column = Column.objects.create(board=board, order=order, name=name, wip_limit=wip_limit)
            column.save()

            subcolumn = Subcolumn.objects.create(column=column, order=1, name="Subcolumn 1",
                                                 is_border_column=is_border_column, is_highest_priority_card_column=is_highest_priority_card_column,
                                                 is_acceptance_testing_column=is_acceptance_testing_column)
            subcolumn.save()

            return render(request, self.template_name)
        else:
            return render(request, self.template_name, {"permission_error": "TODO: change this .... form is not valid"})

def update_column(request, column_id):
    template_name = 'boards/update_column.html'

    column = get_object_or_404(Column, pk=column_id)
    old_wip_limit = column.wip_limit
    update_column_form = forms.UpdateColumnForm(request.POST or None, instance=column)
    if update_column_form.is_valid():
        new_name = update_column_form.cleaned_data['name']
        new_wip_limit = update_column_form.cleaned_data['wip_limit']
        num_of_cards_in_column = utilities.no_of_cards_in_column(column)
        if new_wip_limit is not None and column.wip_limit is not None and \
            new_wip_limit != old_wip_limit and \
            num_of_cards_in_column > new_wip_limit:
            return render(request, template_name, {"new_name": new_name,
                                                   "new_wip_limit": new_wip_limit,
                                                   "column_id": column_id,
                                                   "violation": True},)

        update_column_form.save()
        return render(request, template_name)
    column_wip_violations = WIPColumnViolation.objects.filter(column=column)
    return render(request, template_name, {"update_column_form": update_column_form,
                                           "column_id": column_id,
                                           "column_wip_violations": column_wip_violations})

def update_column_violation(request, column_id, new_name, new_wip_limit):
    template_name = 'display_message.html'

    column = get_object_or_404(Column, pk=column_id)
    column.name = new_name
    column.wip_limit = new_wip_limit
    column.save()

    WIPColumnViolation.objects.create(column=column, date=datetime.datetime.now(), user=request.user)

    #return render(request, template_name, {"message": "Change successful. WIP violation reported."})
    return HttpResponseRedirect(reverse('boards'))

"""
def wip_column_violation_confirmation(request, column_id, new_name, new_wip_limit):
    # ADD WIP VIOLATION HERE.

    instance = get_object_or_404(Column, pk=column_id)
    instance.name = new_name
    instance.wip_limit = new_wip_limit
    instance.save()

    return HttpResponseRedirect(reverse('boards'))
"""

def collapse_column(request, column_id):

    instance = get_object_or_404(Column, pk=column_id)
    if instance.collapsed:
        instance.collapsed = False
    else:
        instance.collapsed = True
    instance.save()
    return redirect('%s%s' % ("/boards/display_board/", instance.board.id))

def delete_column(request, column_id):
    template_name = 'display_message.html'

    column = get_object_or_404(Column, pk=column_id)
    if not UserStoryCard.objects.filter(subcolumn__column=column).exists():
        order = column.order

        Subcolumn.objects.filter(column=column).delete()
        column.delete()

        # Fix columns' order to -1 for every column right of the deleted one.
        Column.objects.filter(order__gte=order).update(order=F('order') - 1)

        return render(request, template_name, {"message": "Column was successfully deleted."})
    return render(request, template_name, {"message": "You can not delete this column as it contains at least one card."})

def create_swim_lane(request, board_id):
    template_name = 'boards/create_swim_lane.html'

    create_swim_lane_form = forms.CreateSwimLaneForm(request.POST or None)
    if create_swim_lane_form.is_valid():
        board = get_object_or_404(Board, pk=board_id)
        project = create_swim_lane_form.cleaned_data['project']

        order = 1 # Default order if no swim lanes exist yet
        if SwimLane.objects.filter(board=board).exists():
            order = SwimLane.objects.filter(board=board).aggregate(Max('order'))['order__max'] + 1

        swim_lane = SwimLane.objects.create(board=board, project=project, order=order)
        swim_lane.save()
        return render(request, template_name)
    return render(request, template_name, {"create_swim_lane_form": create_swim_lane_form})

def delete_swim_lanes(request, board_id):
    template_name = 'boards/delete_swim_lanes.html'

    board = get_object_or_404(Board, pk=board_id)
    swim_lanes = SwimLane.objects.filter(board=board)

    swimLane_project = []
    for swim_lane in swim_lanes:
        swimLane_project.append( (swim_lane, Project.objects.get(pk=swim_lane.project_id)) )

    return render(request, template_name, {"swimLane_project": swimLane_project})

def delete_swim_lane(request, swim_lane_id):
    template_name = 'display_message.html'

    swim_lane = get_object_or_404(SwimLane, pk=swim_lane_id)
    swim_lane.delete()
    return render(request, template_name, {"message": "Swim lane was successfully deleted from the board."})

def create_subcolumn(request, column_id, order):
    template_name = 'boards/update_subcolumn.html'

    update_subcolumn_form = forms.UpdateSubcolumnForm(request.POST or None)
    if update_subcolumn_form.is_valid():
        column = get_object_or_404(Column, pk=column_id)
        name = update_subcolumn_form.cleaned_data['name']
        is_border_column = update_subcolumn_form.cleaned_data['is_border_column']
        is_highest_priority_card_column = update_subcolumn_form.cleaned_data['is_highest_priority_card_column']
        is_acceptance_testing_column = update_subcolumn_form.cleaned_data['is_acceptance_testing_column']

        # Move everything to right.
        if Subcolumn.objects.filter(order=order, column=column).exists():
            Subcolumn.objects.filter(order__gte=order, column=column).update(order=F('order') + 1)

        subcolumn = Subcolumn.objects.create(column=column, order=order, name=name, is_border_column=is_border_column,
                                             is_highest_priority_card_column=is_highest_priority_card_column,
                                             is_acceptance_testing_column=is_acceptance_testing_column)
        subcolumn.save()
        return render(request, template_name, {"message": "Subcolumn successfully created."})
    return render(request, template_name, {"update_subcolumn_form": update_subcolumn_form})

def update_subcolumn(request, subcolumn_id):
    template_name = 'boards/update_subcolumn.html'

    instance = get_object_or_404(Subcolumn, pk=subcolumn_id)
    update_subcolumn_form = forms.UpdateSubcolumnForm(request.POST or None, instance=instance)
    if update_subcolumn_form.is_valid():
        update_subcolumn_form.save()
        return render(request, template_name, {"message": "Subcolumn successfully updated."})
    return render(request, template_name, {"update_subcolumn_form": update_subcolumn_form,
                                           "subcolumn_id": subcolumn_id,
                                           "is_update": True
                                           })

def delete_subcolumn(request, subcolumn_id):
    template_name = 'display_message.html'

    subcolumn = get_object_or_404(Subcolumn, pk=subcolumn_id)
    if UserStoryCard.objects.filter(subcolumn=subcolumn).exists():
        return render(request, template_name, {"message": "You can not delete this subcolumn as it contains at least one card."})
    elif Subcolumn.objects.filter(column=subcolumn.column).count() == 1:
        return render(request, template_name, {"message": "You can not delete this subcolumn as it is the only one in the column."
                                                          "You can delete the column instead."})
    order = subcolumn.order
    column = subcolumn.column
    subcolumn.delete()

    Subcolumn.objects.filter(order__gte=order, column=column).update(order=F('order') - 1)

    return render(request, template_name, {"message": "Subcolumn was successfully deleted."})

def board_properties(request, board_id):
    template_name = 'boards/board_properties.html'

    board = get_object_or_404(Board, pk=board_id)
    board_properties_form = forms.BoardPropertiesForm(request.POST or None, instance=board)
    if board_properties_form.is_valid():
        board_properties_form.save()
        return render(request, template_name)
    return render(request, template_name, {"board_properties_form": board_properties_form,
                                            })

class Users(View):
    template_name = 'users.html'

    @access_required('admin')
    def get(self, request, *args, **kwargs):
        users = UserProfile.objects.all()
        user_roles = []

        for user in users:
            roles = []
            user_profile = UserProfile.objects.get(user=user)
            if user_profile.can_be_kbm:
                roles.append("Kanban Master")
            if user_profile.can_be_po:
                roles.append("Product Owner")
            if user_profile.can_be_dp:
                roles.append("Developer")
            user_roles.append([user,roles])
        return render(request, self.template_name, {"users": user_roles})


class Help(View):
    template_name = 'help.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class AddUser(View):
    template_name = 'add_user.html'

    @access_required('admin')
    def get(self, request):
        roles = {"Kanban Master", "Product Owner", "Developer"}
        return render(request, self.template_name, {"roles": roles})

    @access_required('admin')
    def post(self, request):
        roles = {"Kanban Master", "Product Owner", "Developer"}
        if User.objects.filter(username=request.POST["email"]).count():
            return render(request, self.template_name, {"error": "User with this username (email) already exists.","roles": roles})
        else:
            username = request.POST["email"]
            password = request.POST["password"]
            password_repeat = request.POST["password_repeat"]

            if len(username) < 4:
                return render(request, self.template_name, {"error": "Username should be at least 4 characters long!",
                                                            "roles": roles})

            if len(password) < 4:
                return render(request, self.template_name, {"error": "Password should be at least 4 characters long!",
                                                            "roles": roles})

            try:
                validate_email(username)
            except ValidationError:
                return render(request, self.template_name, {"error": "Email not valid!", "roles": roles})

            if not password == password_repeat:
                return render(request, self.template_name, {"error": "Passwords did not match!", "roles": roles})

            user = User.objects.create(username=username)
            user.set_password(password)
            user_profile = UserProfile.objects.create(user=user)

            if request.POST.get("KanbanMaster", 0):
                user_profile.can_be_kbm = True
            if request.POST.get("ProductOwner", 0):
                user_profile.can_be_po = True
            if request.POST.get("Developer", 0):
                user_profile.can_be_dp = True
            if request.POST.get("admin", 0):
                user.is_superuser = True
            user.save()
            user_profile.save()

            return render(request, self.template_name, {"success": "User was successfully created.", "roles": roles})


class EditUser(View):
    template_name = 'edit_user.html'

    @access_required('admin')
    def get(self, request, user_id):
        user_ = User.objects.get(pk=int(user_id))
        user_profile = UserProfile.objects.get(user=user_)
        if request.GET.get("deactivate", "n") == "t":
            user_.is_active = False
            user_.save()
            return redirect("users")
        elif request.GET.get("activate", "n") == "t":
            user_.is_active = True
            user_.save()
            return redirect("users")

        return render(request, self.template_name, {"user_": user_, "user_profile": user_profile,
                                                    "user_id": user_id})

    @access_required('admin')
    def post(self, request, user_id):
        user_ = User.objects.get(pk=int(user_id))
        user_profile = UserProfile.objects.get(user=user_)

        if request.POST.get("first_name", 0):
            user_.first_name = request.POST["first_name"]
        if request.POST.get("last_name", 0):
            user_.last_name = request.POST["last_name"]
        if request.POST.get("email", 0):
            user_.username = request.POST["email"]
        if request.POST.get("admin", 0):
            user_.is_superuser = True

        if request.POST.get("password", 0) or request.POST.get("password_repeat", 0):
            if request.POST["password"] == request.POST["password_repeat"]:
                user_.password = request.POST["password"]
            else:
                return render(request, self.template_name, {"user_": user_, "user_profile": user_profile,
                                                        "user_id": user_id, "error": "Passwords did not match!"})
            if not request.POST.get("password", 0) or not request.POST.get("password_repeat", 0):
                return render(request, self.template_name, {"user_": user_, "user_profile": user_profile,
                                                            "user_id": user_id, "error": "Please repeat the password."})
        user_profile.can_be_kbm = True if request.POST.get("KanbanMaster", 0) else False
        user_profile.can_be_po = True if request.POST.get("ProductOwner", 0) else False
        user_profile.can_be_dp = True if request.POST.get("Developer", 0) else False
        user_.save()
        user_profile.save()
        return render(request, self.template_name, {"user_": user_, "user_profile": user_profile,
                                                    "user_id": user_id, "success": "User successfully edited."})


#TODO: preveri pravice kjer je edini pogoj da si prijavljen
class WipList(View):
    template_name = 'wip_list.html'

    def post(self, request, board_id):

        #We will reuse form from cumulative flow diagram
        form = forms.CumulativeFlowDiagramForm(request.POST, board_id=board_id)

        if form.is_valid():
            cleaned_form = form.cleaned_data
            date_from = cleaned_form['date_from']
            date_to = cleaned_form['date_to']
            project = cleaned_form['project']
            creation_date_from = cleaned_form['creation_date_from']
            creation_date_to = cleaned_form['creation_date_to']
            finished_date_from = cleaned_form['finished_date_from']
            finished_date_to = cleaned_form['finished_date_to']
            actual_development_from = cleaned_form['actual_development_from']
            actual_development_to = cleaned_form['actual_development_to']
            number_of_points_from = cleaned_form['number_of_points_from']
            number_of_points_to = cleaned_form['number_of_points_to']
            card_types = cleaned_form['card_types']

            cards = utilities.get_cards_in_board(board_id)
            if creation_date_from:
                cards = cards.filter(date_created__gte=creation_date_from)
            if creation_date_to:
                cards = cards.filter(date_created__lte=creation_date_to)
            if project:
                cards = cards.filter(project=project)
            if finished_date_from:
                cards = cards.filter(date_to_finish__gte=finished_date_from)
            if finished_date_to:
                cards = cards.filter(date_to_finish__lte=finished_date_to)
            if actual_development_from:
                cards = cards.filter(date_created__gte=actual_development_from)
            if actual_development_to:
                cards = cards.filter(date_created__lte=actual_development_to)
            if number_of_points_from:
                cards = cards.filter(points__gte=number_of_points_from)
            if number_of_points_to:
                cards = cards.filter(points__lte=number_of_points_to)
            temp_cards = UserStoryCard.objects.none()
            if len(card_types) != 0:
                for type in CARD_TYPES:
                    if type[0] in card_types:
                        temp_cards = cards.filter(type=type[0]) | temp_cards
                cards = temp_cards
            wips = WIPViolation.objects.filter(card__in=cards, date__gte=date_from, date__lte=date_to).order_by('date')

            board = Board.objects.get(pk=board_id)


            return render(request, self.template_name, {'board': board, 'wips':wips})
        else:
            return render(request, self.template_name, {'form': form})


    def get(self, request, board_id):
        form = forms.CumulativeFlowDiagramForm(board_id=board_id)

        board = Board.objects.get(pk=board_id)
        return render(request, self.template_name, {'form': form, 'board': board})

class AverageLeadTime(View):
    template_name = 'average_lead_time.html'

    def post(self, request, board_id):
        form = forms.AverageLeadTimeForm(request.POST, board_id=board_id)

        if form.is_valid():
            cleaned_form = form.cleaned_data
            column_from = cleaned_form['column_from']
            column_to = cleaned_form['column_to']
            project = cleaned_form['project']
            creation_date_from = cleaned_form['creation_date_from']
            creation_date_to = cleaned_form['creation_date_to']
            finished_date_from = cleaned_form['finished_date_from']
            finished_date_to = cleaned_form['finished_date_to']
            actual_development_from = cleaned_form['actual_development_from']
            actual_development_to = cleaned_form['actual_development_to']
            number_of_points_from = cleaned_form['number_of_points_from']
            number_of_points_to = cleaned_form['number_of_points_to']
            card_types = cleaned_form['card_types']

            cards = utilities.get_cards_in_board(board_id)
            if creation_date_from:
                cards = cards.filter(date_created__gte=creation_date_from)
            if creation_date_to:
                cards = cards.filter(date_created__lte=creation_date_to)
            if project:
                cards = cards.filter(project=project)
            if finished_date_from:
                cards = cards.filter(date_to_finish__gte=finished_date_from)
            if finished_date_to:
                cards = cards.filter(date_to_finish__lte=finished_date_to)
            if actual_development_from:
                cards = cards.filter(date_created__gte=actual_development_from)
            if actual_development_to:
                cards = cards.filter(date_created__lte=actual_development_to)
            if number_of_points_from:
                cards = cards.filter(points__gte=number_of_points_from)
            if number_of_points_to:
                cards = cards.filter(points__lte=number_of_points_to)
            temp_cards = UserStoryCard.objects.none()
            if len(card_types) != 0:
                for type in CARD_TYPES:
                    if type[0] in card_types:
                        temp_cards = cards.filter(type=type[0]) | temp_cards

                cards = temp_cards

            lead_time = datetime.timedelta(seconds=0)
            i = 0
            for card in cards:
                card.lead_time = utilities.lead_time(column_from=column_from, column_to=column_to, card=card)
                if card.lead_time != -1 and card.lead_time != -2:
                    lead_time += card.lead_time
                    i += 1
            if i != 0:
                lead_time /= i
                lead_time = datetime.timedelta(seconds=((lead_time.seconds + lead_time.days * 24 * 3600)))
            return render(request, self.template_name, {'cards': cards,
                                                        'column_from': column_from,
                                                        'column_to': column_to,
                                                        'lead_time': lead_time})
        else:
            return render(request, self.template_name, {'form': form})


    def get(self, request, board_id):
        team = utilities.get_team_of_board(board_id=board_id)
        kbm = utilities.get_team_kanbanMaster(team=team)
        if request.user.userprofile != kbm:
            message = "Only kanban master is allowed to see average lead time."
            return render(request, 'display_message.html', {'message': message})

        form = forms.AverageLeadTimeForm(board_id=board_id)

        return render(request, self.template_name, {'form': form})


class CumulativeFlowDiagram(View):
    template_name = 'cumulative_flow_diagram.html'

    def post(self, request, board_id):
        form = forms.CumulativeFlowDiagramForm(request.POST, board_id=board_id)

        if form.is_valid():
            cleaned_form = form.cleaned_data
            date_from = cleaned_form['date_from']
            date_to = cleaned_form['date_to']
            project = cleaned_form['project']
            creation_date_from = cleaned_form['creation_date_from']
            creation_date_to = cleaned_form['creation_date_to']
            finished_date_from = cleaned_form['finished_date_from']
            finished_date_to = cleaned_form['finished_date_to']
            actual_development_from = cleaned_form['actual_development_from']
            actual_development_to = cleaned_form['actual_development_to']
            number_of_points_from = cleaned_form['number_of_points_from']
            number_of_points_to = cleaned_form['number_of_points_to']
            card_types = cleaned_form['card_types']

            cards = utilities.get_cards_in_board(board_id)
            if creation_date_from:
                cards = cards.filter(date_created__gte=creation_date_from)
            if creation_date_to:
                cards = cards.filter(date_created__lte=creation_date_to)
            if project:
                cards = cards.filter(project=project)
            if finished_date_from:
                cards = cards.filter(date_to_finish__gte=finished_date_from)
            if finished_date_to:
                cards = cards.filter(date_to_finish__lte=finished_date_to)
            if actual_development_from:
                cards = cards.filter(date_created__gte=actual_development_from)
            if actual_development_to:
                cards = cards.filter(date_created__lte=actual_development_to)
            if number_of_points_from:
                cards = cards.filter(points__gte=number_of_points_from)
            if number_of_points_to:
                cards = cards.filter(points__lte=number_of_points_to)

            temp_cards = UserStoryCard.objects.none()
            if len(card_types) != 0:
                for type in CARD_TYPES:
                    if type[0] in card_types:
                        temp_cards = cards.filter(type=type[0]) | temp_cards

                cards = temp_cards

            diagram_data = utilities.get_cards_for_diagram(cards, date_from, date_to)
            board = Board.objects.get(pk=board_id)
            return render(request, self.template_name, {'board': board, 'diagram_data': diagram_data})
        else:
            return render(request, self.template_name, {'form': form})

    def get(self, request, board_id):
        team = utilities.get_team_of_board(board_id=board_id)
        kbm = utilities.get_team_kanbanMaster(team=team)

        if request.user.userprofile != kbm:
            message = "Only kanban master is allowed to see cumulative flow diagram."
            return render(request, 'display_message.html', {'message': message})

        form = forms.CumulativeFlowDiagramForm(board_id=board_id)

        board = Board.objects.get(pk=board_id)
        return render(request, self.template_name, {'form': form, 'board': board})


class TaskDistributionDiagram(View):
    template_name = 'task_distribution_diagram.html'

    def post(self, request, board_id):
        form = forms.TaskDistributionForm(request.POST, board_id=board_id)

        if form.is_valid():
            cleaned_form = form.cleaned_data
            project = cleaned_form['project']
            creation_date_from = cleaned_form['creation_date_from']
            creation_date_to = cleaned_form['creation_date_to']
            finished_date_from = cleaned_form['finished_date_from']
            finished_date_to = cleaned_form['finished_date_to']
            actual_development_from = cleaned_form['actual_development_from']
            actual_development_to = cleaned_form['actual_development_to']
            number_of_points_from = cleaned_form['number_of_points_from']
            number_of_points_to = cleaned_form['number_of_points_to']
            card_types = cleaned_form['card_types']

            cards = utilities.get_cards_in_board(board_id)
            if creation_date_from:
                cards = cards.filter(date_created__gte=creation_date_from)
            if creation_date_to:
                cards = cards.filter(date_created__lte=creation_date_to)
            if project:
                cards = cards.filter(project=project)
            if finished_date_from:
                cards = cards.filter(date_to_finish__gte=finished_date_from)
            if finished_date_to:
                cards = cards.filter(date_to_finish__lte=finished_date_to)
            if actual_development_from:
                cards = cards.filter(date_created__gte=actual_development_from)
            if actual_development_to:
                cards = cards.filter(date_created__lte=actual_development_to)
            if number_of_points_from:
                cards = cards.filter(points__gte=number_of_points_from)
            if number_of_points_to:
                cards = cards.filter(points__lte=number_of_points_to)

            temp_cards = UserStoryCard.objects.none()
            if len(card_types) != 0:
                for type in CARD_TYPES:
                    if type[0] in card_types:
                        temp_cards = cards.filter(type=type[0]) | temp_cards

                cards = temp_cards
            developers = utilities.get_task_distribution_data(board=board_id, cards=cards)
            board = Board.objects.get(pk=board_id)

            return render(request, self.template_name, {'board': board, 'developers': developers})
        else:
            return render(request, self.template_name, {'form': form})

    def get(self, request, board_id):
        team = utilities.get_team_of_board(board_id=board_id)
        kbm = utilities.get_team_kanbanMaster(team=team)

        if request.user.userprofile != kbm:
            message = "Only kanban master is allowed to see task distribution diagram."
            return render(request, 'display_message.html', {'message': message})
        form = forms.TaskDistributionForm(board_id=board_id)

        board = Board.objects.get(pk=board_id)
        return render(request, self.template_name, {'form': form, 'board': board})
