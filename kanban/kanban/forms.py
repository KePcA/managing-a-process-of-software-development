
from django import forms
from django.db import models
from django.core.exceptions import ValidationError
from kanban.models import *
from django.contrib.auth.models import User
import copy
from django.forms.extras.widgets import SelectDateWidget
import utilities


"""
CHOICES - Here are the all the data for choices in form fields
"""
#PRODUCT_OWNERS_CREATE = ((user.id, user.user.first_name+" "+user.user.last_name+"-"+user.user.username) for user in UserProfile.objects.filter(can_be_po=True))
#KANBAN_MASTERS_CREATE = ((user.id, user.user.first_name+" "+user.user.last_name+"-"+user.user.username) for user in UserProfile.objects.filter(can_be_kbm=True))
#DEVELOPERS = ((user.id, user.user.first_name+" "+user.user.last_name+"-"+user.user.username) for user in UserProfile.objects.filter(can_be_dp=True))
#PRODUCT_OWNERS_UPDATE = ((user.id, user.user.first_name+" "+user.user.last_name+"-"+user.user.username) for user in UserProfile.objects.filter(can_be_po=True))
#KANBAN_MASTERS_UPDATE = ((user.id, user.user.first_name+" "+user.user.last_name+"-"+user.user.username) for user in UserProfile.objects.filter(can_be_kbm=True))


#Returns all users which can be ProductOwners
def all_product_owners():
    return ((user.id, user.user.first_name+" "+user.user.last_name+" ("+user.user.username+")") for user in UserProfile.objects.filter(can_be_po=True, user__is_staff=False, user__is_active=True))


#Returns all users which can be KanbanMasters
def all_kanban_masters():
    return ((user.id, user.user.first_name+" "+user.user.last_name+" ("+user.user.username+")") for user in UserProfile.objects.filter(can_be_kbm=True, user__is_staff=False, user__is_active=True))


#Returns all users which can be Developers
def all_developers():
    return ((user.id, user.user.first_name+" "+user.user.last_name+" ("+user.user.username+")") for user in UserProfile.objects.filter(can_be_dp=True, user__is_staff=False, user__is_active=True))


#Returns all developers in team
def get_team_developers(team):
    team_developers = []
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "Developer":
                team_developers.append((team_member.member.id, team_member.member.user.first_name+" "+team_member.member.user.last_name+" ("+team_member.member.user.username+")"))
                break
    return tuple(team_developers)


#Returns all developers which are not in the team
def get_other_developers(team):
    #TEAM_DEVELOPERS = get_team_developers(team)
    other_developers = []
    team_developers = []
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "Developer":
                team_developers.append(team_member.member)
                break
    all_developers = UserProfile.objects.filter(can_be_dp=True, user__is_staff=False, user__is_active=True)
    for developer in all_developers:
        if developer not in team_developers:
            other_developers.append((developer.id, developer.user.first_name+" "+developer.user.last_name+" ("+developer.user.username+")"))
    return tuple(other_developers)


#Returns team KanbanMaster
def get_team_kbm(team):
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "KanbanMaster":
                return team_member.member.id
    return None


#Returns team ProductOwner
def get_team_po(team):
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "ProductOwner":
                return team_member.member.id
    return None


#Returns all teams
def get_all_teams():
    return ((team.id, team.team_name) for team in Team.objects.all())


def get_teams_user_is_kbm(user_kmb):
    teams = []
    kbm_role = Role.objects.get(role_name="KanbanMaster")
    for member_team in MemberTeam.objects.filter(member=user_kmb, is_active=True):
        if MemberTeamRole.objects.filter(member_team=member_team, role=kbm_role):
            if member_team.team not in teams:
                teams.append(member_team.team)
    return ((team.id, team.team_name) for team in teams)

#Returns first highest priority subcolumn
def get_higest_priority_subcolumn(board):
    for column in Column.objects.filter(board=board):
        for subcolumn in Subcolumn.objects.filter(column=column):
            if subcolumn.is_highest_priority_card_column:
                return subcolumn
    return None

#Returns all possible subcolumns to move card after declined story (highest priority one and all the left from it)
def get_subcolumns_for_declined_story(board):
    highest_priority_subcolumn = get_higest_priority_subcolumn(board)
    all_subcolumns = []
    if highest_priority_subcolumn != None:
        subcolumn = highest_priority_subcolumn
        column = subcolumn.column

        while True:
            all_subcolumns.append(subcolumn)
            if column.order == 1 and subcolumn.order == 1:
                break

            elif subcolumn.order > 1:
                subcolumn = Subcolumn.objects.get(column=column, order=subcolumn.order-1)

            elif subcolumn.order == 1:
                column = Column.objects.get(board=board, order=column.order-1)
                subcolumn = Subcolumn.objects.get(column=column, order=len(Subcolumn.objects.filter(column=column)))

    else:
        column = Column.objects.get(board=board, order=1)
        all_subcolumns.append(Subcolumn.objects.get(column=column, order=1))

    return ((subcolumn.id, subcolumn.column.name + " -> " + subcolumn.name) for subcolumn in all_subcolumns)


def get_board_subcolumns(board):
    subcolumns = []
    columns = Column.objects.filter(board=board)
    for column in columns:
        subcolumns += Subcolumn.objects.filter(column=column)
    return ((subcolumn.id, subcolumn.column.name + " -> " + subcolumn.name) for subcolumn in subcolumns)

"""
VALIDATOR - Here are all the methods user for validating forms
"""
#Checks if team name is already taken (for team creation)
def team_name_taken(name):
    if Team.objects.filter(team_name=name):
        raise ValidationError('Team name already taken.')

def board_name_taken(name):
    if Board.objects.filter(name=name):
        raise ValidationError('Board name already taken.')

"""
FORMS - All the classes representing forms
"""

#Form for creating new team
class CreateTeamForm(forms.Form):
    team_name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=100, validators=[team_name_taken])

    def __init__(self, *args, **kwargs):
        super(CreateTeamForm, self).__init__(*args, **kwargs)
        self.fields['product_owner'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=all_kanban_masters())
        self.fields['kanban_master'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=all_kanban_masters())
        self.fields['developers'] = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'class' : 'form-control'}), choices=all_developers())


#Form for updating the team
class UpdateTeamForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.team = kwargs.pop('team')
        super(UpdateTeamForm, self).__init__(*args, **kwargs)

        self.fields['product_owner'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=all_product_owners(), initial=get_team_po(self.team))
        self.fields['kanban_master'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=all_kanban_masters(), initial=get_team_kbm(self.team))
        self.fields['add_developers'] = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'class' : 'form-control'}), choices=get_other_developers(self.team), required=False)
        self.fields['remove_developers'] = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'class' : 'form-control'}), choices=get_team_developers(self.team), required=False)


    def clean(self):
        cleaned_data = super(UpdateTeamForm, self).clean()
        #Error if all developers were removed and none added
        if not cleaned_data.get('add_developers') and len(cleaned_data.get('remove_developers'))==len(get_team_developers(self.team)):
            raise ValidationError("Error - team would be without any developer!")
        return self.cleaned_data


#Form for creating new project
class CreateProjectForm(forms.Form):

    """
    code = forms.CharField(max_length=50)
    title = forms.CharField(max_length=100)
    client_name = forms.CharField(max_length=100)
    start_date = forms.DateField(widget=SelectDateWidget)
    end_date = forms.DateField(widget=SelectDateWidget)
    team = forms.ChoiceField(choices=get_all_teams())
    """

    def __init__(self, *args, **kwargs):
        self.user_kbm = kwargs.pop('user_kbm')
        super(CreateProjectForm, self).__init__(*args, **kwargs)

        self.fields['code'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=50)
        self.fields['title'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=100)
        self.fields['client_name'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=100)
        self.fields['start_date'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['anticipated_end_date'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['team'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=get_teams_user_is_kbm(self.user_kbm))


    def clean(self):
        cleaned_data = super(CreateProjectForm, self).clean()
        #Error - Project code already exists
        if Project.objects.filter(code=cleaned_data.get('code')):
            self._errors['code'] = self.error_class(["Project code already exists."])
        #Error - Start date cannot be in the future
        if cleaned_data.get('start_date'):
            if cleaned_data.get('start_date') > datetime.date.today():
                self._errors['start_date'] = self.error_class(["Cannot be in the future."])
        #Error - Anticipated end date cannot be before start date
        if cleaned_data.get('anticipated_end_date') and cleaned_data.get('start_date'):
            if cleaned_data.get('anticipated_end_date') < cleaned_data.get('start_date'):
                self._errors['anticipated_end_date'] = self.error_class(["Must be after start date."])

        return self.cleaned_data


#Form for editing project
class EditProjectForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.user_kbm = kwargs.pop('user_kbm')
        self.project = kwargs.pop('project')
        super(EditProjectForm, self).__init__(*args, **kwargs)

        self.fields['code'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'readonly': 'readonly'}), initial=self.project.code, max_length=50)
        self.fields['title'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=self.project.title, max_length=100)
        self.fields['client_name'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=self.project.client_name, max_length=100)
        if self.project.work_started:
            self.fields['start_date'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control', 'readonly': 'readonly'}), initial=self.project.start_date)
        else:
            self.fields['start_date'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=self.project.start_date)
        self.fields['anticipated_end_date'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=self.project.anticipated_end_date)
        self.fields['finished_date'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=self.project.finished_date, required=False)
        self.fields['team'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=get_teams_user_is_kbm(self.user_kbm), initial=self.project.team.id)


    def clean(self):
        cleaned_data = super(EditProjectForm, self).clean()
        #Error - Start date cannot be in the future
        if cleaned_data.get('start_date'):
            if cleaned_data.get('start_date') > datetime.date.today():
                self._errors['start_date'] = self.error_class(["Cannot be in the future."])
        #Error - Finished date cannot be in the past nor today
        if cleaned_data.get('finished_date'):
            if cleaned_data.get('finished_date') <= datetime.date.today():
                self._errors['finished_date'] = self.error_class(["Cannot be in the past"])
        #Error - Anticipated end date cannot be before start date
        if cleaned_data.get('anticipated_end_date') and cleaned_data.get('start_date'):
            if cleaned_data.get('anticipated_end_date') < cleaned_data.get('start_date'):
                self._errors['anticipated_end_date'] = self.error_class(["Must be after start date."])

        return self.cleaned_data


# Form for creating new board.
class CreateBoardForm(forms.Form):
    board_name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=100, validators=[board_name_taken])

# Form for creating new column.
class CreateColumnForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=100)
    wip_limit = forms.IntegerField(min_value=0, required=False)
    is_border_column = forms.BooleanField(required=False)
    is_highest_priority_card_column = forms.BooleanField(required=False)
    is_acceptance_testing_column = forms.BooleanField(required=False)

# Form for updating a column.
class UpdateColumnForm(forms.ModelForm):
    class Meta:
        model = Column
        fields = ['name', 'wip_limit']

class UpdateSubcolumnForm(forms.ModelForm):
    class Meta:
        model = Subcolumn
        fields = ['name', 'is_border_column', 'is_highest_priority_card_column', 'is_acceptance_testing_column']

class CreateSwimLaneForm(forms.ModelForm):
    class Meta:
        model = SwimLane
        fields = ['project']


#Form for selecting sub-column for declined story
class SubcolumnDeclinedStory(forms.Form):

    def __init__(self,  *args, **kwargs):
        self.board = kwargs.pop('board')
        super(SubcolumnDeclinedStory, self).__init__(*args, **kwargs)

        self.fields['subcolumn'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=get_subcolumns_for_declined_story(self.board))


#Form for deleting card - only entering reason
class DeleteCardForm(forms.Form):
    reason = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=200)

#To initialize form = CardCreationForm(projects=<list of projects>)
class CardCreationForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), max_length=100)
    description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control'}), max_length=1000)
    project = forms.ModelChoiceField(Project.objects.none(), widget=forms.Select(attrs={'class' : 'form-control'}))
    priority = forms.ChoiceField(CARD_PRIORITIES, widget=forms.Select(attrs={'class' : 'form-control'}))
    priority.required = False
    points = forms.DecimalField(decimal_places=2, max_digits=5, widget=forms.TextInput(attrs={'class': 'form-control'}))
    date_to_finish = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    responsible_person = forms.ModelChoiceField(UserProfile.objects.none(), widget=forms.Select(attrs={'class' : 'form-control'}), required=False)

    def __init__(self, *args, **kwargs):
        projects = kwargs.pop('projects')
        super(CardCreationForm, self).__init__(*args, **kwargs)

        list_of_ids = []
        team_members = []
        for p in projects:
            list_of_ids.append(p.id)
            team_members = team_members + utilities.get_team_members(p.team)

        user_profile_ids = []
        for m in team_members:
            user_profile_ids.append(m.id);
        self.fields['project'].queryset = Project.objects.filter(pk__in=list_of_ids)
        self.fields['responsible_person'].queryset = UserProfile.objects.filter(pk__in=user_profile_ids)

#To initialize form = CardEditForm(projects=<list of projects>, card=<card>)
class CardEditForm(forms.Form):


    def __init__(self, *args, **kwargs):
        projects = kwargs.pop('projects')
        card_id = kwargs.pop('card')
        super(CardEditForm, self).__init__(*args, **kwargs)

        card = UserStoryCard.objects.get(pk=card_id)
        self.fields['title'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=card.title, max_length=100)
        self.fields['description'] = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control'}), initial=card.description, max_length=1000)
        self.fields['project'] = forms.ModelChoiceField(Project.objects.none(), widget=forms.Select(attrs={'class' : 'form-control'}))
        self.fields['priority'] = forms.ChoiceField(CARD_PRIORITIES, widget=forms.Select(attrs={'class' : 'form-control'}), initial=card.priority)
        self.fields['priority'].required = False
        if (card.type == 'sb'):
            self.fields['priority'].initial = 'high'
            self.fields['priority'].widget.attrs['disabled'] = 'disabled'

        self.fields['points'] = forms.DecimalField(decimal_places=2, max_digits=5, widget=forms.TextInput(attrs={'class' : 'form-control'}), initial=card.points)
        self.fields['date_to_finish'] = forms.DateField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['responsible_person'] = forms.ModelChoiceField(UserProfile.objects.none(), widget=forms.Select(attrs={'class' : 'form-control'}),
                                                                   required = False, initial = card.responsible_person)

        list_of_ids = []
        team_members = []
        for p in projects:
            list_of_ids.append(p.id)
            team_members = team_members + utilities.get_team_members(p.team)

        user_profile_ids = []
        for m in team_members:
            user_profile_ids.append(m.id);

        self.fields['project'].queryset = Project.objects.filter(pk__in=list_of_ids)
        self.fields['project'].initial = card.project

        self.fields['date_to_finish'].initial = card.date_to_finish
        self.fields['responsible_person'].queryset = UserProfile.objects.filter(pk__in=user_profile_ids)
        self.fields['responsible_person'].initial = card.responsible_person




#Form for creating card movement permission
class CreateCardMovementPermission(forms.Form):

    def __init__(self, *args, **kwargs):
        self.board = kwargs.pop('board')
        super(CreateCardMovementPermission, self).__init__(*args, **kwargs)

        self.fields['subcolumn_from'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=get_board_subcolumns(self.board))
        self.fields['subcolumn_to'] = forms.ChoiceField(widget=forms.Select(attrs={'class' : 'form-control'}), choices=get_board_subcolumns(self.board))
        self.fields['po_allowed'] = forms.BooleanField(required=False)
        self.fields['kbm_allowed'] = forms.BooleanField(required=False)
        self.fields['dp_allowed'] = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = super(CreateCardMovementPermission, self).clean()
        if CardMovementPermission.objects.filter(subcolumn_from=Subcolumn.objects.get(pk=cleaned_data.get('subcolumn_from')), subcolumn_to=Subcolumn.objects.get(pk=cleaned_data.get('subcolumn_to'))):
            raise ValidationError("Card movement permission already exists.")
        #Error - subcolumn_to must be different than subcolumn_from
        if cleaned_data.get('subcolumn_from') == cleaned_data.get('subcolumn_to'):
            self._errors['subcolumn_to'] = self.error_class(["Must differ."])
        #Error - at least one role must be selected
        if not (cleaned_data.get('po_allowed') or cleaned_data.get('kbm_allowed') or cleaned_data.get('dp_allowed')):
            self._errors['po_allowed'] = self.error_class(["Must pick at least one."])
        if cleaned_data.get('po_allowed') and cleaned_data.get('kbm_allowed') and cleaned_data.get('dp_allowed'):
            self._errors['po_allowed'] = self.error_class(["Already set all by default."])

        return cleaned_data


#Form for creating card movement permission
class EditCardMovementPermission(forms.Form):

    def __init__(self, *args, **kwargs):
        self.board = kwargs.pop('board')
        super(EditCardMovementPermission, self).__init__(*args, **kwargs)

        self.fields['subcolumn_from'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'readonly': 'readonly'}))
        self.fields['subcolumn_to'] = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'readonly': 'readonly'}))
        self.fields['po_allowed'] = forms.BooleanField(required=False)
        self.fields['kbm_allowed'] = forms.BooleanField(required=False)
        self.fields['dp_allowed'] = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = super(EditCardMovementPermission, self).clean()
        #Error - at least one role must be selected
        if not (cleaned_data.get('po_allowed') or cleaned_data.get('kbm_allowed') or cleaned_data.get('dp_allowed')):
            self._errors['po_allowed'] = self.error_class(["Must pick at least one."])
        if cleaned_data.get('po_allowed') and cleaned_data.get('kbm_allowed') and cleaned_data.get('dp_allowed'):
            self._errors['po_allowed'] = self.error_class(["Already set all by default."])

        return cleaned_data

#Form for creating task for the card
class CreateTask(forms.Form):
    description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control'}), max_length=1000, required=True)
    hours = forms.IntegerField()

    def clean(self):
        cleaned_data = super(CreateTask, self).clean()
        try:
            if not cleaned_data.get('hours'):
                self._errors['hours'] = self.error_class(["This field is required."])
            else:
                hours = int(cleaned_data.get('hours'))
                if hours <= 0:
                    self._errors['hours'] = self.error_class(["Must be at least 1."])
        except ValueError:
            self._errors['hours'] = self.error_class(["Must be a positive integer."])
        return cleaned_data

#Form for editing task for the card
class EditTask(forms.Form):
    description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control'}), max_length=1000, required=True)
    hours = forms.IntegerField()
    completed = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = super(EditTask, self).clean()
        try:
            if not cleaned_data.get('hours'):
                self._errors['hours'] = self.error_class(["This field is required."])
            else:
                hours = int(cleaned_data.get('hours'))
                if hours <= 0:
                    self._errors['hours'] = self.error_class(["Must be at least 1."])
        except ValueError:
            self._errors['hours'] = self.error_class(["Must be a positive integer."])
        return cleaned_data


class AverageLeadTimeForm(forms.Form):
    column_from = forms.ModelChoiceField(Column.objects.none(), widget=forms.Select(attrs={'class': 'form-control'}))
    column_to = forms.ModelChoiceField(Column.objects.none(),widget=forms.Select(attrs={'class': 'form-control'}))
    project = forms.ModelChoiceField(Project.objects.none(), widget=forms.Select(attrs={'class': 'form-control'}), required=False)
    creation_date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    creation_date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    finished_date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    finished_date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    actual_development_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    actual_development_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    number_of_points_from = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    number_of_points_to = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    card_types = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CARD_TYPES, required=False)

    def __init__(self, *args, **kwargs):
        board_id = kwargs.pop('board_id')
        super(AverageLeadTimeForm, self).__init__(*args, **kwargs)

        if SwimLane.objects.filter(board_id=board_id).count()>0:
            swim_lanes = SwimLane.objects.filter(board_id=board_id)

        projects = []
        for s in swim_lanes:
            projects.append(s.project)
        list_of_ids = []
        for p in projects:
            list_of_ids.append(p.id)

        self.fields['project'].queryset = Project.objects.filter(pk__in=list_of_ids)
        board = Board.objects.get(pk=board_id)
        columns = columns = Column.objects.filter(board=board)
        self.fields['column_from'].queryset = columns
        self.fields['column_to'].queryset = columns

    def clean(self):
        cleaned_data = super(AverageLeadTimeForm, self).clean()
        column_from = cleaned_data.get('column_from')
        column_to = cleaned_data.get('column_to')
        if column_from and column_to and (column_to.order < column_from.order):
            self._errors['column_to'] = self.error_class(["Column to must be more on right or equal to column from."])
        return self.cleaned_data


class CumulativeFlowDiagramForm(forms.Form):
    date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    project = forms.ModelChoiceField(Project.objects.none(), widget=forms.Select(attrs={'class': 'form-control'}), required=False)
    creation_date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    creation_date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    finished_date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    finished_date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    actual_development_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    actual_development_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    number_of_points_from = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    number_of_points_to = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    card_types = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CARD_TYPES, required=False)

    def __init__(self, *args, **kwargs):
        board_id = kwargs.pop('board_id')
        super(CumulativeFlowDiagramForm, self).__init__(*args, **kwargs)

        if SwimLane.objects.filter(board_id=board_id).count()>0:
            swim_lanes = SwimLane.objects.filter(board_id=board_id)

        projects = []
        for s in swim_lanes:
            projects.append(s.project)
        list_of_ids = []
        for p in projects:
            list_of_ids.append(p.id)

        self.fields['project'].queryset = Project.objects.filter(pk__in=list_of_ids)

    def clean(self):
        cleaned_data = super(CumulativeFlowDiagramForm, self).clean()
        date_from = cleaned_data.get('date_from')
        date_to = cleaned_data.get('date_to')
        if date_from and date_to and (date_from > date_to):
            self._errors['date_from'] = self.error_class(["Date from must be before date to."])
        elif date_from and (date_from > datetime.date.today()):
            self._errors['date_to'] = self.error_class(["Date to should not be in the future."])
        if date_to and (date_to > datetime.date.today()):
            self._errors['date_to'] = self.error_class(["Date to should not be in the future."])
        return self.cleaned_data

class TaskDistributionForm(forms.Form):
    project = forms.ModelChoiceField(Project.objects.none(), widget=forms.Select(attrs={'class': 'form-control'}), required=False)
    creation_date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    creation_date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    finished_date_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    finished_date_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    actual_development_from = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    actual_development_to = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    number_of_points_from = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    number_of_points_to = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    card_types = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CARD_TYPES, required=False)

    def __init__(self, *args, **kwargs):
        board_id = kwargs.pop('board_id')
        super(TaskDistributionForm, self).__init__(*args, **kwargs)

        if SwimLane.objects.filter(board_id=board_id).count()>0:
            swim_lanes = SwimLane.objects.filter(board_id=board_id)

        projects = []
        for s in swim_lanes:
            projects.append(s.project)
        list_of_ids = []
        for p in projects:
            list_of_ids.append(p.id)

        self.fields['project'].queryset = Project.objects.filter(pk__in=list_of_ids)

    def clean(self):
        cleaned_data = super(TaskDistributionForm, self).clean()
        date_from = cleaned_data.get('date_from')
        date_to = cleaned_data.get('date_to')
        if date_from and date_to and (date_from > date_to):
            self._errors['date_from'] = self.error_class(["Date from must be before date to."])
        elif date_from and (date_from > datetime.date.today()):
            self._errors['date_to'] = self.error_class(["Date to should not be in the future."])
        if date_to and (date_to > datetime.date.today()):
            self._errors['date_to'] = self.error_class(["Date to should not be in the future."])
        return self.cleaned_data

class BoardPropertiesForm(forms.ModelForm):
    class Meta:
        model = Board
        fields = ['should_display_priority', 'should_display_points', 'should_display_date_to_finish',
                  'n_days_reminder']
