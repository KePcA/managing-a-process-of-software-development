#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.contrib.auth.models import User
from django.views.generic import View
from django.http import HttpResponse
from django.utils import timezone
from kanban.models import UserProfile, Board, Column, Subcolumn, SwimLane, UserStoryCard, Role, Project, Team, CardMovement, WIPViolation, WIP_REASONS
from django.contrib.auth import create_superuser
from django.db import transaction
import datetime

class PopulateDatabase(View):
    def get(self, request):
        if User.objects.filter(username="admin", password="16smrpo").count():
            return HttpResponse("User exists. Delete and sync database before using this view.")

        user = User.objects.create_superuser(username="admin@admin.si", password="16smrpo", email="test@test.si")
        user.save()

        """
        user1 = User.objects.create(username="igorklepic@gmail.com", first_name="Igor", last_name="Klepic", email="igorklepic@gmail.com")
        user1.set_password("igorklepic");
        user1.save();
        user_profile1 = UserProfile.objects.create(user=user1, can_be_po=False, can_be_kbm=False, can_be_dp=True)
        user_profile1.save
        """
        user_profile = UserProfile(user=user)
        user_profile.can_be_po = True
        user_profile.can_be_dp = True
        user_profile.can_be_kbm = True
        user_profile.save()

        #
        # TEAMS
        #

        team1 = Team.objects.create(team_name="Ekipa 1")

        #
        # PROJECTS
        #

        project1 = Project.objects.create(code="P1", title="Projekt 1", client_name="Janez", start_date=timezone.now(), anticipated_end_date=timezone.now(),
                                          team=team1)
        project1.save()

        project2 = Project.objects.create(code="P2", title="Projekt 2", client_name="Lojze", start_date=timezone.now(), anticipated_end_date=timezone.now(),
                                          team=team1)
        project2.save()

        project3 = Project.objects.create(code="P3", title="Projekt 3", client_name="Franci", start_date=timezone.now(), anticipated_end_date=timezone.now(),
                                          team=team1)
        project3.save()

        #
        # BOARDS
        #

        board = Board.objects.create(name="Example board")
        board.save()

        column_productBacklog = Column.objects.create(board=board, order=1, name="Product Backlog")
        column_productBacklog.save()
        column_next = Column.objects.create(board=board, order=2, name="Next", wip_limit=4)
        column_next.save()
        column_development = Column.objects.create(board=board, order=3, name="Development", wip_limit=8)
        column_development.save()
        column_acceptanceReady = Column.objects.create(board=board, order=4, name="Acceptance Ready", wip_limit=2)
        column_acceptanceReady.save()
        column_acceptance = Column.objects.create(board=board, order=5, name="Acceptance", wip_limit=2)
        column_acceptance.save()
        column_done = Column.objects.create(board=board, order=6, name="Done")
        column_done.save()

        subcolumn_productBacklog = Subcolumn.objects.create(column=column_productBacklog, order=1)
        subcolumn_productBacklog.save()
        subcolumn_next = Subcolumn.objects.create(column=column_next, order=1)
        subcolumn_next.save()

        subcolumn_analysisDesign = Subcolumn.objects.create(column=column_development, order=1, name="Analysis & Design")
        subcolumn_analysisDesign.save()
        subcolumn_coding = Subcolumn.objects.create(column=column_development, order=2, name="Coding")
        subcolumn_coding.save()
        subcolumn_testing = Subcolumn.objects.create(column=column_development, order=3, name="Testing")
        subcolumn_testing.save()
        subcolumn_integration = Subcolumn.objects.create(column=column_development, order=4, name="Integration")
        subcolumn_integration.save()
        subcolumn_documentation = Subcolumn.objects.create(column=column_development, order=5, name="Documentation")
        subcolumn_documentation.save()

        subcolumn_acceptanceReady = Subcolumn.objects.create(column=column_acceptanceReady, order=1)
        subcolumn_acceptanceReady.save()
        subcolumn_acceptance = Subcolumn.objects.create(column=column_acceptance, order=1)
        subcolumn_acceptance.save()
        subcolumn_done = Subcolumn.objects.create(column=column_done, order=1)
        subcolumn_done.save()

        swim_lane = SwimLane.objects.create(board=board, order=1, project=project1)
        swim_lane.save()

        swim_lane = SwimLane.objects.create(board=board, order=2, project=project2)
        swim_lane.save()

        #
        # CARDS
        #
        endTime = timezone.now() + datetime.timedelta(days=1)
        card1 = UserStoryCard.objects.create(title="A card", description="Very interesting card (medium priority)", subcolumn=subcolumn_coding,
                                            project=project1, type='nc', priority='high', points=10, date_to_finish=endTime)
        card1.save()

        card11 = UserStoryCard.objects.create(title="A card 11", description="Very interesting card 11 (medium priority)", subcolumn=subcolumn_coding,
                                             project=project1, type='nc', priority='high', points=10, date_to_finish=endTime)
        card11.save()

        card2 = UserStoryCard.objects.create(title="Another card", description="Wazaaap (medium priority)", subcolumn=subcolumn_coding,
                                            project=project2, type='nc', priority='high', points=10, date_to_finish=endTime)
        card2.save()

        card3 = UserStoryCard.objects.create(title="Urgent card 1", description="Auto generated urgent requirement (high priority)", subcolumn=subcolumn_documentation,
                                             project=project1, type='sb', priority='med', points=10, date_to_finish=endTime)
        card3.save()

        #Fixed roles
        Role.objects.create(role_name="ProductOwner")
        Role.objects.create(role_name="KanbanMaster")
        Role.objects.create(role_name="Developer")


        """
        Sample users with diferent roles
        """
        #Igor Klepic - dp
        user = User.objects.create(username="igorklepic@gmail.com", first_name="Igor", last_name="Klepič")
        user.set_password("igorklepic")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=False, can_be_kbm=False, can_be_dp=True)
        #Ban Ana - dp
        user = User.objects.create(username="ban.ana@gmail.com", first_name="Ana", last_name="Ban")
        user.set_password("banana")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=False, can_be_kbm=False, can_be_dp=True)
        #Janez Novak - dp
        user = User.objects.create(username="janez.novak@gmail.com", first_name="Janez", last_name="Novak")
        user.set_password("janeznovak")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=False, can_be_kbm=False, can_be_dp=True)
        #Jelka Smrekar - dp
        user = User.objects.create(username="jelka.smreka@gmail.com", first_name="Jelka", last_name="Smreka")
        user.set_password("jelkasmreka")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=False, can_be_kbm=False, can_be_dp=True)
        #Luka Fuerst - po, kbm
        user = User.objects.create(username="luka.fuerst@gmail.com", first_name="Luka", last_name="Fuerst")
        user.set_password("lukafuerst")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=True, can_be_kbm=True, can_be_dp=False)
        #Marko Poženel - kbm, dp
        user = User.objects.create(username="marko.pozenel@gmail.com", first_name="Marko", last_name="Poženel")
        user.set_password("markopozenel")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=False, can_be_kbm=True, can_be_dp=True)
        #Anže Časar - kbm
        user = User.objects.create(username="anze.casar@gmail.com", first_name="Anže", last_name="Časar")
        user.set_password("anzecasar")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=False, can_be_kbm=True, can_be_dp=False)
        #Viljan Mahnič - po, kbm, dp
        user = User.objects.create(username="viljan.mahnic@gmail.com", first_name="Viljan", last_name="Mahnič")
        user.set_password("viljanmahnic")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=True, can_be_kbm=True, can_be_dp=True)
        #Bill Gates - po
        user = User.objects.create(username="bill.gates@gmail.com", first_name="Bill", last_name="Gates")
        user.set_password("billgates")
        user.save()
        UserProfile.objects.create(user=user, can_be_po=True, can_be_kbm=False, can_be_dp=False)

        return HttpResponse("Database populated.")

#After creating team and some projects in application, create board and some cards
class PopulateDatabaseTeamBoard(View):

    def get(self, request):

        smurfs_team = Team.objects.get(team_name="The smurfs")

        azrael_project = Project.objects.get(team=smurfs_team, code="AZR")
        gargamel_project = Project.objects.get(team=smurfs_team, code="GRG")
        dam_project = Project.objects.get(team=smurfs_team, code="DAM")



        smurfs_board = Board.objects.create(name="Smurfs board")
        smurfs_board.save()

        column_productBacklog = Column.objects.create(board=smurfs_board, order=1, name="Product Backlog")
        column_productBacklog.save()
        column_next = Column.objects.create(board=smurfs_board, order=2, name="Next", wip_limit=4)
        column_next.save()
        column_development = Column.objects.create(board=smurfs_board, order=3, name="Development", wip_limit=8)
        column_development.save()
        column_acceptanceReady = Column.objects.create(board=smurfs_board, order=4, name="Acceptance Ready", wip_limit=2)
        column_acceptanceReady.save()
        column_acceptance = Column.objects.create(board=smurfs_board, order=5, name="Acceptance", wip_limit=2)
        column_acceptance.save()
        column_done = Column.objects.create(board=smurfs_board, order=6, name="Done")
        column_done.save()

        subcolumn_productBacklog = Subcolumn.objects.create(column=column_productBacklog, order=1, name="Product Backlog")
        subcolumn_productBacklog.save()
        subcolumn_next = Subcolumn.objects.create(column=column_next, order=1, name="Next", is_highest_priority_card_column=True)
        subcolumn_next.save()

        subcolumn_analysisDesign = Subcolumn.objects.create(column=column_development, order=1, name="Analysis & Design", is_border_column=True)
        subcolumn_analysisDesign.save()
        subcolumn_coding = Subcolumn.objects.create(column=column_development, order=2, name="Coding")
        subcolumn_coding.save()
        subcolumn_testing = Subcolumn.objects.create(column=column_development, order=3, name="Testing")
        subcolumn_testing.save()
        subcolumn_integration = Subcolumn.objects.create(column=column_development, order=4, name="Integration")
        subcolumn_integration.save()
        subcolumn_documentation = Subcolumn.objects.create(column=column_development, order=5, name="Documentation", is_border_column=True)
        subcolumn_documentation.save()

        subcolumn_acceptanceReady = Subcolumn.objects.create(column=column_acceptanceReady, order=1, name="Acceptance Ready")
        subcolumn_acceptanceReady.save()
        subcolumn_acceptance = Subcolumn.objects.create(column=column_acceptance, order=1,  name="Acceptance", is_acceptance_testing_column=True)
        subcolumn_acceptance.save()
        subcolumn_done = Subcolumn.objects.create(column=column_done, order=1, name="Done")
        subcolumn_done.save()

        swim_lane = SwimLane.objects.create(board=smurfs_board, order=1, project=azrael_project)
        swim_lane.save()

        swim_lane = SwimLane.objects.create(board=smurfs_board, order=2, project=gargamel_project)
        swim_lane.save()

        #
        # CARDS
        #

        avg_card1 = UserStoryCard.objects.create(title="Azrael card 1: - Azrael", description="Very interesting card (medium priority)", subcolumn=subcolumn_productBacklog, project=azrael_project, type='nc', priority='high', points=10)
        avg_card1.save()

        avg_card2 = UserStoryCard.objects.create(title="Gargamel card: 2 - Gargamel", description="Wazaaap (medium priority)", subcolumn=subcolumn_productBacklog, project=gargamel_project, type='nc', priority='high', points=8)
        avg_card2.save()

        avg_card3 = UserStoryCard.objects.create(title="Azrael card: 3 - Azrael", description="Auto generated urgent requirement (high priority)", subcolumn=subcolumn_productBacklog, project=azrael_project, type='sb', priority='med', points=6)
        avg_card3.save()

        avg_card4 = UserStoryCard.objects.create(title="Azrael card: 4 - Azrael", description="Auto generated urgent requirement (high priority)", subcolumn=subcolumn_productBacklog, project=azrael_project, type='sb', priority='med', points=2)
        avg_card4.save()

        avg_card5 = UserStoryCard.objects.create(title="Azrael card: 5 - Azrael", description="Auto generated urgent requirement (high priority)", subcolumn=subcolumn_productBacklog, project=azrael_project, type='sb', priority='med', points=10)
        avg_card5.save()

        avg_card6 = UserStoryCard.objects.create(title="Azrael card: 6 - Azrael", description="Auto generated urgent requirement (high priority)", subcolumn=subcolumn_productBacklog, project=azrael_project, type='nc', priority='high', points=4)
        avg_card6.save()

        avg_card7 = UserStoryCard.objects.create(title="Gargamel card: 7 - Gargamel", description="Wazaaap (medium priority)", subcolumn=subcolumn_productBacklog, project=gargamel_project, type='ub', priority='med', points=5)
        avg_card7.save()

        return HttpResponse("Database populated.")


class PopulateCardMovements(View):


    def get(self, request):

        smurfs_team = Team.objects.get(team_name="The smurfs")
        azrael_project = Project.objects.get(team=smurfs_team, code="AZR")
        gargamel_project = Project.objects.get(team=smurfs_team, code="GRG")

        card = UserStoryCard.objects.get(title="Azrael card 1: - Azrael")
        board = card.subcolumn.column.board
        column_productBacklog = Column.objects.get(board=board, name="Product Backlog")
        subcolumn_productBacklog = Subcolumn.objects.get(column=column_productBacklog, name="Product Backlog")

        column_acceptanceReady = Column.objects.get(board=board, name="Acceptance Ready")
        subcolumn_acceptanceReady = Subcolumn.objects.get(column=column_acceptanceReady, name="Acceptance Ready")
        column_done = Column.objects.get(board=board, name="Done")
        subcolumn_done = Subcolumn.objects.get(column=column_done, name="Done")

        #
        # CARDS
        #
        card = UserStoryCard.objects.create(title="Normal moving card (to the end)", description="Very interesting card", subcolumn=subcolumn_productBacklog, project=azrael_project, type='nc', priority='medium', points=10, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))
        card1 = UserStoryCard.objects.create(title="Two front, one back moving card (to the end)", description="Very interesting card", subcolumn=subcolumn_productBacklog, project=azrael_project, type='nc', priority='low', points=5, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))
        card2 = UserStoryCard.objects.create(title="Silver bullet moving card (to the end)", description="Very interesting card ", subcolumn=subcolumn_productBacklog, project=azrael_project, type='sb', priority='high', points=3, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))
        card3 = UserStoryCard.objects.create(title="Normal card (to the middle)", description="Very interesting card ", subcolumn=subcolumn_productBacklog, project=azrael_project, type='nc', priority='medium', points=4, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))
        card4 = UserStoryCard.objects.create(title="Silver bullet (to the middle)", description="Very interesting card", subcolumn=subcolumn_productBacklog, project=azrael_project, type='sb', priority='high', points=4, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))

        card23 = UserStoryCard.objects.create(title="Normal card (to the middle)", description="Very interesting card ", subcolumn=subcolumn_productBacklog, project=gargamel_project, type='nc', priority='medium', points=4, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))
        card24 = UserStoryCard.objects.create(title="Silver bullet (to the middle)", description="Very interesting card", subcolumn=subcolumn_productBacklog, project=gargamel_project, type='sb', priority='high', points=4, date_created=(datetime.datetime.today()+datetime.timedelta(days=-32)))

        #
        # CARD MOVEMENTS
        #
        columns = Column.objects.filter(board=board)
        user = User.objects.get(username="viljan.mahnic@gmail.com")

        #card moving straight to the end
        for i in range(0, len(columns)-1):
            CardMovement.objects.create(card=card, column_from=columns[i], column_to=columns[i+1], user=user,
                                                   date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i)))
            CardMovement.objects.create(card=card2, column_from=columns[i], column_to=columns[i+1], user=user,
                                                   date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i)))

        #card moving two front one back
        for i in range(0, len(columns)-2):
            CardMovement.objects.create(card=card1, column_from=columns[i], column_to=columns[i+1], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i*3)))
            CardMovement.objects.create(card=card1, column_from=columns[i+1], column_to=columns[i+2], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*(i*3+1))))
            CardMovement.objects.create(card=card1, column_from=columns[i+2], column_to=columns[i+1], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*(i*3+2))))
        #last move
        CardMovement.objects.create(card=card1, column_from=columns[len(columns)-2], column_to=columns[len(columns)-1], user=user,
                                    date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*(len(columns)-1)*3+2)))
        card.subcolumn = subcolumn_done
        card2.subcolumn = subcolumn_done
        card1.subcolumn = subcolumn_done
        card.save()
        card2.save()
        card1.save()
        #cards moving to the  middle
        for i in range(0, len(columns)/2):
            CardMovement.objects.create(card=card3, column_from=columns[i], column_to=columns[i+1], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i)))
            CardMovement.objects.create(card=card4, column_from=columns[i], column_to=columns[i+1], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i*2)))
            CardMovement.objects.create(card=card23, column_from=columns[i], column_to=columns[i+1], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i*3)))
            CardMovement.objects.create(card=card24, column_from=columns[i], column_to=columns[i+1], user=user,
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-30*24+24*i*4)))

        card3.subcolumn = subcolumn_acceptanceReady
        card4.subcolumn = subcolumn_acceptanceReady
        card3.save()
        card4.save()

        card23.subcolumn = subcolumn_acceptanceReady
        card24.subcolumn = subcolumn_acceptanceReady
        card23.save()
        card24.save()

        #wip_violations
        cards = [card, card1, card2, card23, card24, card3]
        i=0
        for card in cards:
            WIPViolation.objects.create(card=card, column=card.subcolumn.column, user=user, reason='cm',
                                        date=(datetime.datetime.today()+datetime.timedelta(hours=-10*24+24*i*2)))
            i += 1
        cards = [card4, card24, card]


        for card in cards:
            WIPViolation.objects.create(card=card, date=datetime.datetime.today(), column=card.subcolumn.column,
                                        user=user,  reason='cc')
        return HttpResponse("Card movements populated")