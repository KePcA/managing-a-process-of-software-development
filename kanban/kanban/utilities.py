from models import *
from django.utils import timezone
from django.core.mail import send_mail
from datetime import date, timedelta
from dateutil.relativedelta import *
import os
from kanbanize import settings

"""
Utility methods for application
"""

#Return team kanban master
def get_team_kanbanMaster(team):
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "KanbanMaster":
                return team_member.member
    return None


#Returns team product owner
def get_team_productOwner(team):
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "ProductOwner":
                return team_member.member
    return None


#Returns team product owner
def get_team_developers(team):
    developers = []
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            if team_member_role.role.role_name == "Developer":
                developers.append(team_member.member)
    return developers


#Returns team members
def get_team_members(team):
    developers = []
    members_in_team = MemberTeam.objects.filter(team=team, is_active=True)
    for team_member in members_in_team:
        for team_member_role in MemberTeamRole.objects.filter(member_team=team_member):
            developers.append(team_member.member)
    return developers


#Return all teams user is involved in - active: only teams he is currently being active in (put false for teams, he used to be active in)
def get_teams_user_in(user_profile, active):
    teams = []
    for member_team in MemberTeam.objects.filter(member=user_profile, is_active=active):
        if member_team.team not in teams:
            teams.append(member_team.team)
    return teams


#Returns all projects user is involved in
def get_projects_user_in(user_profile):
    projects = []
    teams = get_teams_user_in(user_profile, True)
    for team in teams:
        projects += Project.objects.filter(team=team)
    return projects


def get_boards_user_in(user_profile):
    teams = get_teams_user_in(user_profile, True)
    swim_lanes = SwimLane.objects.filter(project__team__in=teams)
    boards = []
    for swim_lane in swim_lanes:
        boards += Board.objects.filter(pk=swim_lane.board_id)

    if user_profile.can_be_kbm:
        for board in Board.objects.all():
            if not board in boards and get_cards_in_board(board).count() == 0:
                boards.append(board)

    return list(set(boards))


#Returns no. of cards in particular column
def no_of_cards_in_column(column):
    res = 0
    for subcolumn in Subcolumn.objects.filter(column=column):
        res += len(UserStoryCard.objects.filter(subcolumn=subcolumn))
    return res


def get_cards_in_board(board):
    cards = UserStoryCard.objects.filter(subcolumn__column__board=board)
    return cards


#Helper function that returns all projects for which a user on a specific board and
#  for specific card type is allowed to add that card type card
def getProjects(user, board_id, card_type):
    user_profile = UserProfile.objects.get(user=user)

    #vsi swimlani
    if SwimLane.objects.filter(board_id=board_id).count() > 0:
        swim_lanes = SwimLane.objects.filter(board_id=board_id)
    else:
        return []

    # if no matching query then user does not have permissions
    try:
        member_teams1 = MemberTeam.objects.filter(member=user_profile)
    except MemberTeam.DoesNotExist:
        return []

    kbm_role = Role.objects.get(role_name="KanbanMaster")
    po_role = Role.objects.get(role_name="ProductOwner")
    dp_role = Role.objects.get(role_name="Developer")

    if (card_type == 'nc'):
        m_team_roles = MemberTeamRole.objects.filter(role=po_role)
    elif (card_type == 'sb'):
        m_team_roles = MemberTeamRole.objects.filter(role=kbm_role)

    #dejanski member teami
    #member_teams2 =
    member_teams2 = []
    for m in m_team_roles:
        if m.member_team in member_teams1:
            member_teams2.append(m.member_team);

    teams = []
    for m in member_teams2:
        teams.append(m.team)

    projects = []
    for s in swim_lanes:
        if s.project.team in teams:
            projects.append(s.project)
    return projects
    return res


#Helper function that returns all projects for which a user on a specific board and
#  for specific card type is allowed to add that card type card
def get_all_projects(user, board_id):
    user_profile = UserProfile.objects.get(user=user)

    #vsi swimlani
    if SwimLane.objects.filter(board_id=board_id).count() > 0:
        swim_lanes = SwimLane.objects.filter(board_id=board_id)
    else:
        return []

    # if no matching query then user does not have permissions
    try:
        member_teams1 = MemberTeam.objects.filter(member=user_profile)
    except MemberTeam.DoesNotExist:
        return []

    kbm_role = Role.objects.get(role_name="KanbanMaster")
    po_role = Role.objects.get(role_name="ProductOwner")
    dp_role = Role.objects.get(role_name="Developer")

    m_team_roles = MemberTeamRole.objects.all()
    #dejanski member teami
    #member_teams2 =
    member_teams2 = []
    for m in m_team_roles:
        if m.member_team in member_teams1:
            member_teams2.append(m.member_team);

    teams = []
    for m in member_teams2:
        teams.append(m.team)

    projects = []
    for s in swim_lanes:
        if s.project.team in teams:
            projects.append(s.project)
    return projects
    return res


#Return all subcolumns of particular board
def get_board_subcolumns(board):
    subcolumns = []
    columns = Column.objects.filter(board=board)
    for column in columns:
        subcolumns += Subcolumn.objects.filter(column=column)
    return subcolumns

def card_in_your_project(card, user_profile):
    projects = get_projects_user_in(user_profile=user_profile)
    allowed = 0
    for p in projects:
        if p == card.project:
            allowed = 1
            break
    return allowed


#returns true or false
def has_role_on_specific_project(role, project):
    team = project.team


#Returns true, if subcolumn is left border subcolumn
def is_left_border_subcolumn(subcolumn):
    if subcolumn.is_border_column:
        column = subcolumn.column

        while True:

            if column.order == 1 and subcolumn.order == 1:
                return True

            elif subcolumn.order > 1:
                subcolumn = Subcolumn.objects.get(column=column, order=subcolumn.order - 1)
                if subcolumn.is_border_column:
                    return False

            elif subcolumn.order == 1:
                column = Column.objects.get(board=column.board, order=column.order - 1)
                subcolumn = Subcolumn.objects.get(column=column, order=len(Subcolumn.objects.filter(column=column)))
                if subcolumn.is_border_column:
                    return False

    else:
        return False


#Checks if subcolumn is right of acceptance testing one
def is_right_of_acc_test_subcolumn(subcolumn):
    column = subcolumn.column
    while True:

        if column.order == 1 and subcolumn.order == 1:
            return False

        elif subcolumn.order > 1:
            subcolumn = Subcolumn.objects.get(column=column, order=subcolumn.order - 1)
            if subcolumn.is_acceptance_testing_column:
                return True

        elif subcolumn.order == 1:
            column = Column.objects.get(board=column.board, order=column.order - 1)
            subcolumn = Subcolumn.objects.get(column=column, order=len(Subcolumn.objects.filter(column=column)))
            if subcolumn.is_acceptance_testing_column:
                return True


def are_board_columns_properties_consistent(board):
    columns = Column.objects.filter(board=board)
    subcolumns = Subcolumn.objects.filter(column__in=columns)

    count_border = 0
    count_highest_priority = 0
    count_acceptance_testing = 0
    for subcolumn in subcolumns:
        if subcolumn.is_border_column == True:
            count_border += 1
        if subcolumn.is_highest_priority_card_column == True:
            count_highest_priority += 1
        if subcolumn.is_acceptance_testing_column == True:
            count_acceptance_testing += 1

    return (count_border == 2) and (count_highest_priority == 1) and (count_acceptance_testing == 1)


def all_tasks_completed(card):
    for task in Task.objects.filter(card=card):
        if not task.completed:
            return False
    return True


#Vrne cas potovanja od enega stolpca do drugega
#V kolikor je kartica levo od column_to vrnemo -2 (lead_time se ne da izracunati)
#V kolikor je bila kartica veckrat v enem stolpcu se za zacetek potovanja
#steje najstarejsi datum za konec pa najmlajsi
def lead_time(column_from, column_to, card):
    if card.subcolumn.column.order < column_to.order:
        return -2
    start_date = 0
    board = card.subcolumn.column.board
    first_column = Column.objects.filter(board=board).order_by('order')[0]
    if column_from == first_column:
        start_date = card.date_created
    else:
        movements_from = CardMovement.objects.filter(card=card, column_to=column_from).order_by('date')
        if movements_from:
            start_date = movements_from[0].date

    movements_to = CardMovement.objects.filter(card=card, column_to=column_to).order_by('-date')
    if column_to == first_column and not movements_to:
        end_date = card.date_created
    else:
        if movements_to:
            end_date = movements_to[0].date

    if start_date and end_date:
        tm = end_date - start_date
        tm = datetime.timedelta(seconds=((tm.seconds + tm.days * 24 * 3600)))
        return tm
    #Card has never been moved
    elif card.subcolumn.column == column_to:
        return datetime.timedelta(seconds=0)
    else:
        return -2


def get_team_of_board(board_id):
    swim_lanes = SwimLane.objects.filter(board=board_id)
    if swim_lanes:
        return swim_lanes[0].project.team
    else:
        return 0


#returns data for drawing cumulative flow diagram
def get_cards_for_diagram(cards, date_from, date_to):
    if len(cards) == 0:
        return []
    if date_to > datetime.date.today():
        date_to = datetime.date.today()

    board = cards[0].subcolumn.column.board

    cards.order_by('date_created')
    columns = Column.objects.filter(board=board)

    diagram_data = []
    delta = datetime.timedelta(days=1)
    movements = CardMovement.objects.filter(card__in=cards).order_by('date')
    date_start = movements[0].date
    date_to = datetime.datetime(date_to.year, date_to.month, date_to.day, tzinfo=timezone.utc)
    date_from = datetime.datetime(date_from.year, date_from.month, date_from.day, tzinfo=timezone.utc)
    d = date_from
    while d <= date_to:
        row = [0] * len(columns)
        diagram_data.append(row)
        d += delta


    d = date_from
    if d>date_start:
        d = date_start
    else:
        diagram_data.append(row)

    i = 0

    row = [0] * len(columns)

    for c in cards:
        if c.date_created <= d:
            row[0] += 1

    while d <= date_to+delta:
        movements = CardMovement.objects.filter(date__gte=d, date__lt=d + delta)
        row2 = list(row)
        for c in cards:
            if d <= c.date_created <= d + delta:
                row2[0] += 1
        for m in movements:
            if m.card in cards:
                index_minus = get_column_index(m.column_from)
                index_plus = get_column_index(m.column_to)
                row2[index_minus] -= 1
                row2[index_plus] += 1

        if d >= date_from:
            diagram_data[i] = row2
            i += 1
        d += delta

        row = row2

    column_array = ['Date']
    for c in columns:
        column_array.append(c.name)
    column_array = list(map(str, column_array))
    final_array = [column_array]

    #final_array.append(['a', 'b', 'c', 'd', 'e', 'f', 'g'])
    i = 0
    for d in diagram_data:
        d.insert(0, str((date_from+i*delta).strftime('%m-%d')))
        final_array.append(d)
        #final_array.append(d)
        i+=1
    if len(final_array) == 2:
        final_array.insert(1, list(final_array[1]))
    return final_array

#Vrne zaporedno stevilko stolpca: Npr. za najbolj levi stolpec vrne 0
def get_column_index(column):
    columns = Column.objects.filter(board=column.board)
    i = 0
    for c in columns:
        if c == column:
            return i
        i += 1

    return -1

def is_allowed_to_create_card(user, board_id, card_type):
    team = get_team_of_board(board_id=board_id)
    kbm = get_team_kanbanMaster(team=team)
    po = get_team_productOwner(team=team)
    if card_type == 'nc' and user.userprofile == po:
        return True
    if card_type == 'sb' and user.userprofile == kbm:
        return True
    return False

def send_mail_reminder():
    print "Sending email reminder."

    for board in Board.objects.all().distinct():
        if board.n_days_reminder is None:
            continue

        for swim_lane in SwimLane.objects.filter(board=board).distinct():
            kbm = get_team_kanbanMaster(swim_lane.project.team)

            subject = "[Reminder] Cards deadline in next " + str(board.n_days_reminder) + " days"
            text = "Cards near deadline on a board " + str(board.name) + ":\n\n"

            num_of_cards_reported = 0
            for card in get_cards_in_board(board):
                if not card.deleted and card.date_to_finish is not None and \
                                        card.date_to_finish >= datetime.datetime.now().date() and \
                                        card.date_to_finish - relativedelta(days=board.n_days_reminder) <= datetime.datetime.now().date():
                    text += str(card.title) + " (" + str(card.responsible_person) + " responsible) has finished date set at " + str(card.date_to_finish) + ".\n"
                    num_of_cards_reported += 1

            if num_of_cards_reported > 0:
                print "Sending email to rok.povsic and " + str(kbm.user.username)
                send_mail(subject, text, 'smrpo16@gmail.com',
                          ['rok.povsic@gmail.com', str(kbm.user.username)], fail_silently=False)


#returns
# -2 if there are not exacly two border subcolumns
# -1 if card is left of border
# 0 if card is inside borders
# 1 if card is right of border
def card_allowed_to_edit(card):
    columns = Column.objects.filter(board_id=card.subcolumn.column.board).order_by('order')
    subcolumns = Subcolumn.objects.filter(column__in=columns, is_border_column=True)
    if len(subcolumns) != 2:
        return -2

    if subcolumns[0].column.order < subcolumns[1].column.order:
        left_border_subcolumn = subcolumns[0]
        right_border_subcolumn = subcolumns[1]
    elif subcolumns[0].column.order > subcolumns[1].column.order:
        left_border_subcolumn = subcolumns[0]
        right_border_subcolumn = subcolumns[1]
    #stolpca sta enaka
    elif subcolumns[0].order < subcolumns[1].order:
        left_border_subcolumn = subcolumns[0]
        right_border_subcolumn = subcolumns[1]
    else:
        left_border_subcolumn = subcolumns[1]
        right_border_subcolumn = subcolumns[0]

    if card.subcolumn.column.order < left_border_subcolumn.column.order:
        return -1
    if card.subcolumn.column.order == left_border_subcolumn.column.order and card.subcolumn.order<left_border_subcolumn.order:
        return -1

    if card.subcolumn.column.order > right_border_subcolumn.column.order:
        return 1
    if card.subcolumn.column.order == right_border_subcolumn.column.order and card.subcolumn.order> right_border_subcolumn.order:
        return 1

    return 0

def get_task_distribution_data(board, cards):
    team = get_team_of_board(board_id=board)
    developers = get_team_members(team=team)
    #cards = get_cards_in_board(board=board)

    data = []
    for d in developers:
        cards_of_developer = cards.filter(responsible_person=d)
        all_points = 0
        for c in cards_of_developer:
            all_points += c.points
        print d
        print all_points
        print len(cards_of_developer)
        d.card_count = len(cards_of_developer)
        d.all_points = all_points
        data.append([d.user.username, d.card_count, d.all_points])
    return data

