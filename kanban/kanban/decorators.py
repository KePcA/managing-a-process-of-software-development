from django.shortcuts import render_to_response
from django.template import RequestContext
from functools import wraps


#class based decorator
def access_required(permission):
    def decorator(func):
        def inner_decorator(obj, request, *args, **kwargs):
            if permission == 'admin':
                if request.user.is_staff:
                    return func(obj, request, *args, **kwargs)
                else:
                    return render_to_response('index.html', {"error": "You do not have access to this content."}, context_instance=RequestContext(request))
        return wraps(func)(inner_decorator)
    return decorator


# function based decorator
# def access_required(permission):
#    def decorator(func):
#        def inner_decorator(request, *args, **kwargs):
#            if permission == 'admin':
#                if request.user.is_staff:
#                    return func(request, *args, **kwargs)
#                else:
#                    return render_to_response('index.html', {"error": "You do not have access to this content."}, context_instance=RequestContext(request))
#        return wraps(func)(inner_decorator)
#    return decorator
